//
//  SplashScreenViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 12/15/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector:  #selector(SplashScreenViewController.displaySplash), userInfo: nil, repeats: false)
    }

    @objc func displaySplash() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

    // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController

        if let slideMenuController = GlobalConstants.AppDelegate.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            UIView.transition(with: GlobalConstants.AppDelegate.window!, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromLeft, animations: {
                GlobalConstants.AppDelegate.window?.rootViewController = slideMenuController
                GlobalConstants.AppDelegate.window?.makeKeyAndVisible()
            }, completion: nil)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
