//
//  LegalNotesViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/23/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class LegalNotesViewController: UIViewController, SINavigationBar {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.barWithTint(UIColor.white, translucent: false, backButtonOptions: SINavigationBarBackButtonOptions(backButtonType: .none))
        self.setNavigationBarItem()
        
        let header = UIImageView(image: UIImage(named:GlobalConstants.HeaderMenuImageName))
        header.frame = CGRect(x: (self.navigationController?.navigationBar.frame.origin.x)!, y: (self.navigationController?.navigationBar.frame.origin.y)!, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)! + 20)
        self.navigationController?.view.addSubview(header)
        
        let burgerImage = UIImageView(image: UIImage(named:GlobalConstants.BurgerMenuImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        burgerImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)!), size: burgerImage.frame.size)
        header.addSubview(burgerImage)
        titleLabel.text = LocalizationConstants.LegalNotesMenuItem.uppercased()
        let lineSeparator = UIView(frame: CGRect(x: 0.0, y: titleLabel.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0))
        lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
        titleLabel.addSubview(lineSeparator)
        
        do {
            let path = Bundle.main.path(forResource: "legal", ofType: "html")
            let text = try String(contentsOfFile:path!, encoding: String.Encoding.utf8)
            webView.loadHTMLString(text, baseURL: nil)
            print(text)
        } catch {
            print(error)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
