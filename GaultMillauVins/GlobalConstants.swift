//
//  GlobalConstants.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/14/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import MapKit

struct GlobalConstants {
    
    static var AppDelegate = UIApplication.shared.delegate as! AppDelegate
    static var wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    
    // Texts
    
    static let FavouriteWinesKey = "Favourites"
    static let WineDetailsSegueIdentifier = "ShowWineDetails"
    static let BaseCellIdentifier = "BaseCell"
    static let WineCellIdentifier = "WineCell"
    static let IncreasingPriceNotificationName = "IncreasingPrice"
    static let DecreasingPriceNotificationName = "DecreasingPrice"
    static let IncreasingVintageNotificationName = "IncreasingYear"
    static let DecreasingVintageNotificationName = "DecreasingYear"
    static let MakeFavouriteNotificationName = "MakeFavourite"
    static let ShowLocationOnMapSegueIdentifier = "ShowLocationOnMap"
    static let DropDownCellNibName = "DropDownCell"
    static let SearchForWineByCriteriaSegueIdentifier = "SearchForWine"
    static let LoadingTextMessage = "Loading data, please wait"
    static let DataLoadedNotificationName = "DidLoadData"
    static let PriceNotAvailableText = "NC"
    static let AddedNewFavouriteWineNotificationName = "addedNewFavourite"
    static let ShowFavouriteWinesSegueName = "ShowFavouriteWine"
    static let AnnotationViewReuseIdentifier = "pin"
    static let TitleCellReuseIdentifier = "TitleCell"
    static let MealsWinesCellReuseIdentifier = "MealsWinesTableViewCell"
    static let ShowWineAppelationForMealSegueName = "ShowWineAppelationsForMeal"
    static let MealCellReuseIdentifier = "MealCell"
    static let WineAppellationCellIdentifier = "WineAppelationTableCell"
    static let ShowWinesForAppelationSegueName = "ShowWinesForAppelation"
    static let MealAppellationCellIdentifier = "MealAppelationTableCell"
    static let DetailsCellIdentifier = "DetailsTableCell"
    static let WineByAppelationTableViewCell = "WineByAppelationTableViewCell"
    static let ShowWineDeatilsForMealSegueName = "ShowWineDetailsForMeal"
    static let ShowDomainOnMapSegueIdentifier = "ShowDomainOnMap"
    static let KeywordCellIdentifier = "KeywordCell"
    static let VintageByRegionCellIdentifier = "VintageByRegionCell"
    static let VintageByYearCellIdentifier = "VintageByYearCell"
    static let ShowAdviceDetailsSegueName = "ShowAdviceDetails"
    static let AdviceCellIdentifier = "AdviceCell"
    
    // Colors
    static let RedColorCgValue = #colorLiteral(red: 0.5849441886, green: 0.09956035763, blue: 0.1162954941, alpha: 1).cgColor
    static let RedColor = #colorLiteral(red: 0.5849441886, green: 0.09956035763, blue: 0.1162954941, alpha: 1)
    static let LightGrayColor = #colorLiteral(red: 0.9136354327, green: 0.9137886167, blue: 0.9136152267, alpha: 1)
    
    // Images
    static let HeaderMenuImageName = "header"
    static let BurgerMenuImageName = "burger"
    static let TriangleDownRedImageName = "triangle_down_red"
    static let TriangleDownWiteImageName = "triangle_down_white"
    static let BackButtonImageName = "back_button"
    static let FavouriteButtonIconImageName = "favourite_button_icon"
    static let UnfavouriteButtonIconImageName = "unfavourite_button_icon"
    static let CustomPointAnnotationImageName = "custom_point_annotation"
    static let TriangleRightWhiteImageName = "triangle_right_white"
    static let FullGlassImageName = "full_glass"
    static let EmptyGlassImageName = "empty_glass"
    static let WineDetailsPinImageName = "wine_details_pin"
    
    // Numbers
    static let ScreenWidth = UIScreen.main.bounds.width
    static let ScreenHeight = UIScreen.main.bounds.height
    static let DropDownCellHeight:CGFloat = 64.0
    static let BaseCellHeight:CGFloat = 84.0
    static let WineCellHeight:CGFloat = 220.0
    static let TitleCellHeight:CGFloat = 60.0
    static let MealWinesCellHeight:CGFloat = 64.0
    static let StandartCellHeight:CGFloat = 44.0
}

// Colors
func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
    return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: 1.0)
}

func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
    return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
}

func rgb(_ rgb: CGFloat) -> UIColor {
    return UIColor(red: rgb / 255.0, green: rgb / 255.0, blue: rgb / 255.0, alpha: 1.0)
}

func rgb(_ rgb: CGFloat, _ alpha: CGFloat) -> UIColor {
    return UIColor(red: rgb / 255.0, green: rgb / 255.0, blue: rgb / 255.0, alpha: alpha)
}

// Size of image
func sizeOfImage(_ strImageName: String!) -> CGSize {
    guard let img = UIImage(named: strImageName) else {
        return CGSize(width: 0.0, height: 0.0)
    }
    
    return img.size
}

