//
//  AdviceListTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/29/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class AdviceListTableViewController: UITableViewController, SINavigationBar {
    var burgerImage:UIImageView!
    var advices = Dictionary<String, String>()
    var adviceKeys = [String]()
    var advice = String()
    var adviceName = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.barWithTint(UIColor.white, translucent: false, backButtonOptions: SINavigationBarBackButtonOptions(backButtonType: .none))
        self.setNavigationBarItem()
        self.tableView.registerCellClass(MealsWinesTableViewCell.self)
        self.tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 0.0, right: 0.0)
        DataManager.shared.fetchAdvices() {
            (completion:Dictionary<String, String>) in
            advices = completion
            adviceKeys = Array(advices.keys)
            tableView.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let header = UIImageView(image: UIImage(named:GlobalConstants.HeaderMenuImageName))
        header.frame = CGRect(x: (self.navigationController?.navigationBar.frame.origin.x)!, y: (self.navigationController?.navigationBar.frame.origin.y)!, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)! + 20)
        self.navigationController?.view.addSubview(header)
        
        burgerImage = UIImageView(image: UIImage(named:GlobalConstants.BurgerMenuImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        burgerImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)!), size: burgerImage.frame.size)
        header.addSubview(burgerImage)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return advices.keys.count + 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.TitleCellReuseIdentifier, for: indexPath)
            cell.textLabel?.text = LocalizationConstants.AdvicesTitleText.uppercased()
            let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0)
            let lineSeparator = UIView(frame: frame)
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.AdviceCellIdentifier, for: indexPath) as! MealsWinesTableViewCell
        let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 2.0, width: GlobalConstants.ScreenWidth, height: 2.0)
        let lineSeparator = UIView(frame: frame)
        lineSeparator.backgroundColor = GlobalConstants.RedColor
        cell.addSubview(lineSeparator)
        cell.selectionStyle = .none
        let advice = adviceKeys[indexPath.row - 1]
        cell.nameLabel.text = advice
        cell.nameLabel.textColor = GlobalConstants.RedColor
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 74.0
        }
        return GlobalConstants.TitleCellHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:MealsWinesTableViewCell = tableView.cellForRow(at: indexPath)! as! MealsWinesTableViewCell
        selectedCell.contentView.backgroundColor = GlobalConstants.RedColor
        selectedCell.nameLabel.textColor = UIColor.white
        selectedCell.arrowImage.image = UIImage(named: GlobalConstants.TriangleRightWhiteImageName)
        advice = advices[adviceKeys[indexPath.row - 1]]!
        adviceName = adviceKeys[indexPath.row - 1]
        self.performSegue(withIdentifier: GlobalConstants.ShowAdviceDetailsSegueName, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.ShowAdviceDetailsSegueName {
            let destination = segue.destination as! AdviceViewController
            destination.adviceName = adviceName
            destination.adviceDescription = advice
        }
    }
}


