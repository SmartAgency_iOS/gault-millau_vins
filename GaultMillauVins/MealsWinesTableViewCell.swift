//
//  MealsWinesTableViewCell.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/24/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class MealsWinesTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.contentView.backgroundColor = UIColor.white
        if(self.reuseIdentifier != GlobalConstants.WineAppellationCellIdentifier) {
            self.nameLabel.textColor = GlobalConstants.RedColor
        } else {
            self.nameLabel.textColor = UIColor.black
        }
        self.arrowImage.image = UIImage(named: "triangle_right_red")

    }

}
