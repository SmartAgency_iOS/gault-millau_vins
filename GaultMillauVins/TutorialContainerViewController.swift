//
//  TutorialContainerViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 1/4/17.
//  Copyright © 2017 Veronika Velkova. All rights reserved.
//

import UIKit

class TutorialContainerViewController: UIViewController {
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(skipTutorial(sender:)), name: NSNotification.Name(rawValue: "skipTutorial"), object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "skipTutorial"), object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? TutorialViewController {
            tutorialPageViewController.tutorialDelegate = self
        }
    }

    @objc func skipTutorial(sender:NSNotification) {
        self.dismiss(animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TutorialContainerViewController: TutorialPageViewControllerDelegate {

    func tutorialPageViewController(tutorialPageViewController: TutorialViewController,
                                    didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }

    func tutorialPageViewController(tutorialPageViewController: TutorialViewController,
                                    didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
}
