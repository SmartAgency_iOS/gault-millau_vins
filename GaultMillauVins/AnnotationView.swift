//
//  AnnotationView.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/30/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import MapKit

class AnnotationView: MKAnnotationView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if (hitView != nil) {
            self.superview?.bringSubviewToFront(self)
        }
        
        return hitView
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds;
        var isInside: Bool = rect.contains(point);
        if(!isInside) {
            for view in self.subviews {
                if view.isKind(of: UIButton.self) {
                    return false
                }
                
            isInside = view.frame.contains(point);
                if isInside {
                    break;
                }
            }
        }
    
            return isInside;
    }
    
}
