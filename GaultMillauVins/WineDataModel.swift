//
//  WineDataModel.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/21/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class WineDataModel: NSObject, NSCoding {
    let wineAppeleation:String
    let wineCuvee:String
    let wineBrand:String
    let wineClassification:String
    let wineNote:String
    let winePrice:NSDecimalNumber
    let wineText:String
    let wineVintage:Int32
    let wineDomainName:String
    let wineRegion:String
    let wineColor:String
    
    required init(wineAppeleation:String, wineCuvee:String, wineBrand:String, wineClassification:String, wineNote:String, winePrice:NSDecimalNumber, wineText:String, wineVintage:Int32, wineDomainName:String, wineRegion:String, wineColor:String) {
        self.wineAppeleation = wineAppeleation
        self.wineCuvee = wineCuvee
        self.wineBrand = wineBrand
        self.wineClassification = wineClassification
        self.wineNote = wineNote
        self.winePrice = winePrice
        self.wineText = wineText
        self.wineVintage = wineVintage
        self.wineDomainName = wineDomainName
        self.wineRegion = wineRegion
        self.wineColor = wineColor
    }
    
    required init(coder decoder: NSCoder) {
        self.wineAppeleation = (decoder.decodeObject(forKey: "wineAppeleation") as! String) 
        self.wineCuvee = (decoder.decodeObject(forKey: "wineCuvee") as! String) 
        self.wineBrand = (decoder.decodeObject(forKey: "wineBrand") as! String) 
        self.wineClassification = decoder.decodeObject(forKey: "wineClassification") as! String
        self.wineNote = decoder.decodeObject(forKey: "wineNote") as! String
        self.winePrice = decoder.decodeObject(forKey: "winePrice") as! NSDecimalNumber
        self.wineText = decoder.decodeObject(forKey: "wineText") as! String
        self.wineVintage = (decoder.decodeInt32(forKey: "wineVintage") as Int32)
        self.wineDomainName = decoder.decodeObject(forKey: "wineDomainName") as! String
        self.wineRegion = (decoder.decodeObject(forKey: "wineRegion") as! String)
        self.wineColor = decoder.decodeObject(forKey: "wineColor") as! String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(wineAppeleation, forKey: "wineAppeleation")
        coder.encode(wineCuvee, forKey: "wineCuvee")
        coder.encode(wineBrand, forKey:"wineBrand")
        coder.encode(wineClassification, forKey:"wineClassification")
        coder.encode(wineNote, forKey:"wineNote")
        coder.encode(winePrice, forKey:"winePrice")
        coder.encode(wineText, forKey:"wineText")
        coder.encode(wineVintage, forKey:"wineVintage")
        coder.encode(wineDomainName, forKey: "wineDomainName")
        coder.encode(wineRegion, forKey: "wineRegion")
        coder.encode(wineColor, forKey: "wineColor")
    }
}
