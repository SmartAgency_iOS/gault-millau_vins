//
//  WineTableViewCell.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/17/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class WineTableViewCell: UITableViewCell {
    @IBOutlet weak var wineDescription: UILabel!
    @IBOutlet weak var wineNoteLabel: UILabel!
    @IBOutlet weak var wineRegionLabel: UILabel!
    @IBOutlet weak var wineAppelationLabel: UILabel!
    @IBOutlet weak var wineTextLabel: UILabel!
    @IBOutlet weak var wineDomainLabel: UILabel!
    @IBOutlet weak var winePriceLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var appellationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        appellationLabel.text = LocalizationConstants.AppellationLabelText
        regionLabel.text = LocalizationConstants.RegionLabelText
        wineDescription.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func makeFavourite(_ sender: UIButton) {
        let buttonRow = sender.tag
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.MakeFavouriteNotificationName), object: buttonRow)
        let favouriteButtonImageName = "favourite_button_icon"
        let unfavouriteButtonIcon = "unfavourite_button_icon"
        if(self.favouriteButton.currentImage == UIImage(named:favouriteButtonImageName)) {
            self.favouriteButton.setImage(UIImage(named: unfavouriteButtonIcon), for: .normal)
        } else {
            self.favouriteButton.setImage(UIImage(named: favouriteButtonImageName), for: .normal)
        }
    }
}
