//
//  SILoadingView.swift
//  SIBase
//
//  Created by Angel Antonov on 4/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation
import UIKit

protocol SILoadingViewRenderer {
    func showLoadingView(_ loadingOptions: SILoadingViewOptions)
    func hideLoadingView()
}

extension SILoadingViewRenderer where Self: UIViewController {
    func showLoadingView(_ loadingOptions: SILoadingViewOptions = SILoadingViewOptions()) {
        let viewHolder = loadingOptions.fullScreen == true ? UIApplication.shared.keyWindow : self.view
        
        if let _ = viewHolder?.viewWithTag(100) as? SILoadingView {
            return
        }
        
        let loadingScreen = SILoadingView(loadingOptions: loadingOptions)
        loadingScreen.tag = 100
        
        viewHolder?.addSubview(loadingScreen)
        
        loadingScreen.shouldAnimate = true
        
        loadingScreen.startAnimating()
    }
    
    func hideLoadingView() {
        var loadingScreen = self.view.viewWithTag(100) as? SILoadingView
        
        if loadingScreen == nil {
            loadingScreen = UIApplication.shared.keyWindow?.viewWithTag(100) as? SILoadingView
        }
        
        if let loadingScreen = loadingScreen {
            if loadingScreen.loadingOptions.animated == true {
                UIView.animate(withDuration: loadingScreen.loadingOptions.animationDuration, animations: { 
                        loadingScreen.alpha = 0.0
                    }, completion: { (b) in
                        loadingScreen.stopAnimating()
                        loadingScreen.removeFromSuperview()
                })
            }
            else {
                loadingScreen.stopAnimating()
                loadingScreen.removeFromSuperview()
            }
        }
    }
}

struct SILoadingViewOptions {
    fileprivate static let defaultColor: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
    
    let message: String
    let backgroundColor: UIColor
    let animated: Bool
    let animationDuration: TimeInterval
    let font: UIFont!
    let fullScreen: Bool!
    
    init(message: String = "", backgroundColor: UIColor = SILoadingViewOptions.defaultColor, animated: Bool = true, animationDuration: TimeInterval = 0.25, font: UIFont = UIFont.systemFont(ofSize: UIFont.systemFontSize), fullScreen: Bool = false) {
        self.message = message
        self.backgroundColor = backgroundColor
        self.animated = animated
        self.animationDuration = animationDuration
        self.font = font
        self.fullScreen = fullScreen
    }
}

class SILoadingView: UIView {
    var loadingOptions: SILoadingViewOptions
    
    var activity: UIActivityIndicatorView!
    var shouldAnimate = false
    
    let screenFrame = CGRect(x: 0, y: 0, width: GlobalConstants.ScreenWidth, height: GlobalConstants.ScreenHeight)
    
    init (loadingOptions: SILoadingViewOptions) {
        self.loadingOptions = loadingOptions
        
        super.init(frame : screenFrame)
        
        if loadingOptions.animated == true {
            self.alpha = 0.0
        }
        
        addBehavior()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.loadingOptions = SILoadingViewOptions()
        
        super.init(coder: aDecoder)
    }
    
    func addBehavior() {
        self.backgroundColor = self.loadingOptions.backgroundColor
        
        activity = UIActivityIndicatorView(style: .white)
        activity.center = self.center
        
        self.addSubview(activity)
        
        if self.loadingOptions.message.isEmpty == false {
            let lblLoadingMessage = UILabel(frame: CGRect(x: 0.0, y: activity.bottomYPoint + 6.0, width: GlobalConstants.ScreenWidth, height: 20.0))
            lblLoadingMessage.textAlignment = .center
            lblLoadingMessage.text = self.loadingOptions.message
            lblLoadingMessage.textColor = UIColor.white
            lblLoadingMessage.font = loadingOptions.font
            
            if lblLoadingMessage.neededHeight > lblLoadingMessage.frame.size.height {
                lblLoadingMessage.adjustHeight()
            }
            
            self.addSubview(lblLoadingMessage)
        }
        
        if self.loadingOptions.animated == true {
            UIView.animate(withDuration: self.loadingOptions.animationDuration, animations: {
                self.alpha = 1.0
            })
        }
    }
    
    func startAnimating() {
        if !self.shouldAnimate {
            return
        }
        
        activity.startAnimating()
    }
    
    func stopAnimating() {
        shouldAnimate = false
        
        activity.stopAnimating()
    }
}
