//
//  Operators.swift
//
//  Created by Angel Antonov on 8/26/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

// MARK: - Background ~> Main thread operator

import UIKit
import Foundation

infix operator ~>~

/**
 Custom operator for triggering block of code after some delay in the main thread.
 */
func ~>~ (
    delayInSeconds: TimeInterval,
    mainClosure: @escaping () -> ()
    )
{
    let popTime = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    
    DispatchQueue.main.asyncAfter(deadline: popTime) {
        mainClosure()
    }
}

infix operator ~>

private let queue = DispatchQueue(label: "serial-worker", attributes: [])

func ~> (
    backgroundClosure: @escaping () -> (),
    mainClosure: @escaping () -> ()
    )
{
    queue.async {
        backgroundClosure()
        DispatchQueue.main.async(execute: mainClosure)
    }
}
infix operator >~>

func >~> (
    firstClosure: () -> (),
    secondClosure: (closure: () -> (), delay: Double)
    )
{
    firstClosure()
    let delayInSeconds = secondClosure.delay
    let popTime = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    
    DispatchQueue.main.asyncAfter(deadline: popTime) {
        secondClosure.closure()
    }
}

infix operator /^
func /^ (width1: CGFloat, width2: CGFloat) -> CGFloat {
    return (width1 - width2) / 2.0
}

infix operator ?=?
func ?=? (date1: Date, date2: Date) -> Bool {
    let dateComponents1 = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: date1)
    let dateComponents2 = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: date2)
    
    return dateComponents1.day == dateComponents2.day && dateComponents1.month == dateComponents2.month && dateComponents1.year == dateComponents2.year
}

infix operator ?>?
func ?>? (date1: Date, date2: Date) -> Bool {
    let dateComponents1 = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: date1)
    let dateComponents2 = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: date2)
    
    if dateComponents1.year! > dateComponents2.year! {
        return true
    }
    else if dateComponents1.year! < dateComponents2.year! {
        return false
    }
    
    if dateComponents1.month! > dateComponents2.month! {
        return true
    }
    else if dateComponents1.month! < dateComponents2.month! {
        return false
    }
    
    if dateComponents1.day! > dateComponents2.day! {
        return true
    }
    else if dateComponents1.day! < dateComponents2.day! {
        return false
    }
    
    
    return false
}

infix operator ?<?
func ?<? (date1: Date, date2: Date) -> Bool {
    let dateComponents1 = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: date1)
    let dateComponents2 = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: date2)
    
    if dateComponents1.year! < dateComponents2.year! {
        return true
    }
    else if dateComponents1.year! > dateComponents2.year! {
        return false
    }
    
    if dateComponents1.month! < dateComponents2.month! {
        return true
    }
    else if dateComponents1.month! > dateComponents2.month! {
        return false
    }
    
    if dateComponents1.day! < dateComponents2.day! {
        return true
    }
    else if dateComponents1.day! > dateComponents2.day! {
        return false
    }
    
    
    return false
}

// MARK: - Percentage Operators

// MARK: Work with percentages

infix operator *%
func *% (number: Double, percentage: Double) -> Double {
    return number * (percentage / 100)
}
func *% (number: CGFloat, percentage: CGFloat) -> CGFloat {
    return number * (percentage / 100)
}

infix operator ?%
func ?% (percentage: Double, ofNumber: Double) -> Double {
    return (ofNumber / 100.0) * percentage
}

func ?% (percentage: CGFloat, ofNumber: CGFloat) -> CGFloat {
    return (ofNumber / 100.0) * percentage
}

// MARK: Add/Sum/+

infix operator +%
func +% (number: Double, percentage: Double) -> Double {
    return number + ((number / 100.0) * percentage)
}

infix operator +%=
func +%= (number: inout Double, percentage: Double) {
    number = number +% percentage
}

postfix operator ++%
postfix func ++% (number: inout Double) -> Double{
    let temp = number
    number +%= 1
    return temp
}

prefix operator ++%
prefix func ++% (number: inout Double) -> Double {
    number +%= 1
    return number
}

// MARK: Substract/-

infix operator -%
func -% (number: Double, percentage: Double) -> Double {
    return number - ((number / 100.0) * percentage)
}

func -% (number: CGFloat, percentage: CGFloat) -> CGFloat {
    return number - ((number / 100.0) * percentage)
}

infix operator -%=
func -%= (number: inout Double, percentage: Double) {
    number = number -% percentage
}

postfix operator --%
postfix func --% (number: inout Double) -> Double {
    let temp = number
    number -%= 1
    return temp
}

prefix operator --%
prefix func --% (number: inout Double) -> Double {
    number -%= 1
    return number
}
