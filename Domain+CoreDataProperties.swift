//
//  Domain+CoreDataProperties.swift
//  
//
//  Created by Veronika Velkova on 1/5/17.
//
//

import Foundation
import CoreData


extension Domain {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Domain> {
        return NSFetchRequest<Domain>(entityName: "Domain");
    }

    @NSManaged public var domain_email: String?
    @NSManaged public var domain_name: String?
    @NSManaged public var domain_phone: String?
    @NSManaged public var domain_post_code: String?
    @NSManaged public var domain_website: String?
    @NSManaged public var domain_city: String?
    @NSManaged public var domain_wine: NSSet?
    @NSManaged public var domain_address: Address?

}

// MARK: Generated accessors for domain_wine
extension Domain {

    @objc(addDomain_wineObject:)
    @NSManaged public func addToDomain_wine(_ value: Wine)

    @objc(removeDomain_wineObject:)
    @NSManaged public func removeFromDomain_wine(_ value: Wine)

    @objc(addDomain_wine:)
    @NSManaged public func addToDomain_wine(_ values: NSSet)

    @objc(removeDomain_wine:)
    @NSManaged public func removeFromDomain_wine(_ values: NSSet)

}
