//
//  DataManager.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/15/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import CoreData

final class DataManager: NSObject {
    static let shared = DataManager()
    let dbContext = DataBaseContext.shared.managedObjectContext
    
    private override init() {
    }
    
    func getWineCount(completion: @escaping (_ result: Int) -> Void) {
        var count:Int = 0
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "wine_appelation != nil AND wine_cuvee != nil AND wine_brand != nil AND wine_classification != nil AND wine_domain.domain_name != nil"
        let predicate = NSPredicate(format: format)
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                count = result.count
                completion(count)
                print(count)
            } catch {
                // add code later
            }
        }
    }
    
    func populateRegionDataSource(completion:@escaping (_ result: [String]) -> Void) {
        let request:NSFetchRequest<Region> = Region.fetchRequest()
        var regionNames = [String]()
        self.dbContext.perform {
            do {
                let result = try request.execute()
                result.forEach{ regionNames.append($0.region_name) }
                completion(regionNames)
                print(regionNames)
            } catch {
                // add code later
            }
        }
    }

    func addressesWithoutCoordinates(completion:@escaping (_ result: Int) -> Void) {
        var count:Int = 0
        let request:NSFetchRequest<Address> = Address.fetchRequest()
        let format = "address_name != nil AND address_city != nil AND address_latitude != 0.0000000"
        let predicate = NSPredicate(format: format)
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                count = result.count
                completion(count)
                print(count)
            } catch {
                // add code later
            }
        }
    }

    func allAddresses(completion:@escaping (_ result: Int) -> Void) {
        var count:Int = 0
        let request:NSFetchRequest<Address> = Address.fetchRequest()
        let format = "address_name != nil AND address_city != nil"
        let predicate = NSPredicate(format: format)
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                count = result.count
                completion(count)
                print(count)
            } catch {
                // add code later
            }
        }
    }

    func populateColorDataSource(completion:@escaping (_ result: [String]) -> Void) {
        let request:NSFetchRequest<Color> = Color.fetchRequest()
        var colorNames = [String]()
        self.dbContext.perform {
            do {
                let result = try request.execute()
                result.forEach{ colorNames.append($0.color_name) }
                completion(colorNames)
                print(colorNames)
            } catch {
                // add code later
            }
        }
    }

    func popultaeVintageDataSourceForAllWines(completion:@escaping (_ completion: [String]) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        var years = Set<String>()
        self.dbContext.perform {
            do {
                let result = try request.execute()
                for wine in result {
                    if wine.wine_vintage != 1111 && wine.wine_vintage != 0 {
                        years.insert(String(describing:wine.wine_vintage))
                    }
                }

                completion(Array(years))
            } catch {
                // add code later
            }
        }
    }
    
    func populateVintageDataSource(completion:(_ result: [String]) -> Void) {
        let vintageAsDataArray = UserDefaults.standard.array(forKey: DbConstants.VintageEntityName)
        var vintageArray = [Int]()
        var vintages = [String]()
        let object = vintageAsDataArray!.first
        let dictionary = NSKeyedUnarchiver.unarchiveObject(with: object as! Data)
        let vintage = VintageDataModel(dictionary: dictionary as! Dictionary<String, Any>)
        vintageArray = [Int](vintage.ratingForYear.keys)
        vintageArray = vintageArray.sorted().reversed()
        for v in vintageArray {
            vintages.append(String(v))
        }
        
        completion(vintages)
    }
    
    func populateRegionDataSourceForVintage(completion:(_ result: [String]) -> Void) {
        let vintageAsDataArray = UserDefaults.standard.array(forKey: DbConstants.VintageEntityName)
        var vintages = [String]()
        let objects = vintageAsDataArray
        for object in objects! {
            let dict = NSKeyedUnarchiver.unarchiveObject(with: object as! Data)
            let vintage = VintageDataModel(dictionary: dict as! Dictionary<String, Any>)
            vintages.append(vintage.name)
        }
        
        completion(vintages)
    }
    
    func fetchYearRatingsByRegion(regionCriteria:String, completion:(_ result: Dictionary<Int, Int>) -> Void) {
        let vintageAsDataArray = UserDefaults.standard.array(forKey: DbConstants.VintageEntityName)
        let objects = vintageAsDataArray!
        for object in objects {
            let dict = NSKeyedUnarchiver.unarchiveObject(with: object as! Data)
            let vintage = VintageDataModel(dictionary: dict as! Dictionary<String, Any>)
            if(vintage.name == regionCriteria) {
                completion(vintage.ratingForYear)
            }
        }
    }
    
    func fetchRegionRatingsByYear(vintageCriteria:Int, completion:(_ result: Dictionary<String, Int>) -> Void) {
        let vintageAsDataArray = UserDefaults.standard.array(forKey: DbConstants.VintageEntityName)
        var ratingForRegionName = Dictionary<String, Int>()
        for object in vintageAsDataArray! {
            let dict = NSKeyedUnarchiver.unarchiveObject(with: object as! Data)
            let vintage = VintageDataModel(dictionary: dict as! Dictionary<String, Any>)
            ratingForRegionName[vintage.name] = vintage.ratingForYear[vintageCriteria]
        }
        
        completion(ratingForRegionName)
    }
    
    func fetchAdvices(completion:(_ result: Dictionary<String, String>) -> Void) {
        let advicesAsDataArray = UserDefaults.standard.array(forKey: DbConstants.AdviceEntityName)
        var advices = Dictionary<String, String>()
        for object in advicesAsDataArray! {
            let dict = NSKeyedUnarchiver.unarchiveObject(with: object as! Data)
            let advice = Advice(dictionary: dict as! Dictionary<String, String>)
            advices[advice.name] = advice.description
        }
        
        completion(advices)
    }
    
    func fetchWineByCriteria(wineRegion:String?, wineColor:String?, wineVintage:String?, freeText:String?, completion:@escaping (_ result: [Wine]) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        var predicates = [NSPredicate]()
        if (wineRegion != nil && wineRegion != "") {
            let regionFormat = "wine_region.region_name == %@"
            let regionPredicate = NSPredicate(format: regionFormat, wineRegion!)
            predicates.append(regionPredicate)
        }
        
        if(wineColor != nil && wineColor != "") {
            let colorFormat = "wine_color.color_name == %@"
            let colorPredicate = NSPredicate(format: colorFormat, wineColor!)
            predicates.append(colorPredicate)
        }
        
        if(wineVintage != nil && wineVintage != "") {
            let vintageFormat = "wine_vintage == %@"
            let vintagePredicate = NSPredicate(format: vintageFormat, wineVintage!)
            predicates.append(vintagePredicate)
        }
        
        if(freeText != nil && freeText != "") {
            let freeTextFormat = "wine_appelation contains [cd] %@ OR wine_cuvee contains [cd] %@ OR wine_brand contains [cd] %@ OR wine_classification contains [cd] %@ OR wine_domain.domain_name contains [cd] %@ OR wine_text contains [cd] %@"
            let freeTextPredicate = NSPredicate(format: freeTextFormat, freeText!, freeText!, freeText!, freeText!, freeText!, freeText!)
            predicates.append(freeTextPredicate)
        }
        
        if(predicates.count > 0) {
            let andPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            request.predicate = andPredicate
        } else {
            let format = "wine_appelation != nil AND wine_cuvee != nil AND wine_brand != nil AND wine_classification != nil AND wine_domain.domain_name != nil"
            let predicate = NSPredicate(format: format)
            request.predicate = predicate
        }
        
        self.dbContext.perform {
            do {
                let result = try request.execute()
                completion(result)
                print(result)
            } catch {
                // add code later
            }
        }
    }

    func fetchWineByText(text:String, completion:@escaping (_ result: Wine) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "wine_text contains [cd] %@"
        let predicate = NSPredicate(format: format, text)
        request.predicate = predicate

        self.dbContext.perform {
            do {
                let result = try request.execute().first
                completion(result!)
                print(result!)
            } catch {
                // add code later
            }
        }
    }
    
    func fetchMealsForWine(wine:Wine, completion:@escaping (_ result: [String]) -> Void) {
        let mets = wine.wine_met?.allObjects
        var mealNames = [String]()
        let request:NSFetchRequest<Met> = Met.fetchRequest()
        var predicates = [NSPredicate]()
        print(mets!)
        for met in mets! {
            let format = "met_name = %@"
            let predicate = NSPredicate(format: format, (met as! Met).met_name)
            predicates.append(predicate)
        }
        
        let orPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
        request.predicate = orPredicate
        
        self.dbContext.perform {
            do {
                let result = try request.execute()
                print(result)
                result.forEach({mealNames.append("\($0.met_name) (\($0.met_description.lowercased()))")})
                completion(mealNames)
            } catch {
                // add code later
            }
        }
    }
    
    func fetchMealNamesForWine(wine:Wine, completion:@escaping (_ result: [String]) -> Void) {
        let mets = wine.wine_met?.allObjects
        var mealNames = [String]()
        let request:NSFetchRequest<Met> = Met.fetchRequest()
        var predicates = [NSPredicate]()
        print(mets!)
        for met in mets! {
            let format = "met_name = %@"
            let predicate = NSPredicate(format: format, (met as! Met).met_name)
            predicates.append(predicate)
        }
        
        let orPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
        request.predicate = orPredicate
        
        self.dbContext.perform {
            do {
                let result = try request.execute()
                print(result)
                result.forEach({mealNames.append("\($0.met_name)")})
                completion(mealNames)
            } catch {
                // add code later
            }
        }
    }

    
    func fetchMealsCount(completion:@escaping(_ result:Int) -> Void) {
        let request:NSFetchRequest<Met> = Met.fetchRequest()
        let format = "met_name != nil AND met_description != nil"
        let predicate = NSPredicate(format:format)
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                completion(result.count)
            } catch {
                // add code later
            }
        }
    }

    
    func fetchMeals(completion:@escaping(_ result:[Met]) -> Void) {
        let request:NSFetchRequest<Met> = Met.fetchRequest()
        let format = "met_name != nil AND met_description != nil"
        let predicate = NSPredicate(format:format)
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                completion(result)
            } catch {
                // add code later
            }
        }
    }
    
    func fetchWineAppelationsForMet(met: Met, completion:(_ result:[String]) -> Void) {
        let winesForMeal = met.met_wine.allObjects
        var wineAppelationSet = Set<String>()
        for wine in winesForMeal {
            wineAppelationSet.insert((wine as! Wine).wine_appelation!)
        }
        
        print(wineAppelationSet)
        completion(Array(wineAppelationSet))
    }
    
    func fetchWinesByAppelation(wineAppelation:String, completion:@escaping(_ result:[Wine]) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "wine_appelation == %@"
        let predicate = NSPredicate(format:format, wineAppelation)
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                completion(result)
                print(result)
            } catch {
                // add code later
            }
        }
    }
    
    func fetchChosenFavouriteWine(wine: WineDataModel, completion:@escaping(_ result:Wine) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "wine_appelation == %@ AND wine_cuvee == %@ AND wine_brand == %@ AND wine_classification == %@ AND wine_domain.domain_name == %@ AND wine_region.region_name == %@ AND wine_color.color_name == %@"
        let predicate = NSPredicate(format:format, wine.wineAppeleation, wine.wineCuvee, wine.wineBrand, wine.wineClassification, wine.wineDomainName, wine.wineRegion, wine.wineColor)
        request.predicate = predicate
        
        self.dbContext.perform {
            do {
                let result = try request.execute()
                print(result)
                print(result.first!)
                completion(result.first!)
                
            } catch {
                // add code later
            }
        }
    }

    func fetchWineTypeByRegion(regionName:String, completion:@escaping(_ result:[String]) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "wine_region.region_name == %@"
        let predicate = NSPredicate(format:format, regionName)
        var wineType = Set<String>()
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                print(result)

                for wine in result {
                    wineType.insert(wine.wine_color!.color_name)
                }

                completion(Array(wineType))

            } catch {
                // add code later
            }
        }
    }

    func fetchVintageByRegion(regionName:String, completion:@escaping(_ result:[String]) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "wine_region.region_name == %@"
        let predicate = NSPredicate(format:format, regionName)
        var wineVintage = Set<String>()
        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                print(result)

                for wine in result {
                    if wine.wine_vintage != 1111 {
                        wineVintage.insert(String(describing:wine.wine_vintage))
                    }
                }

                completion(Array(wineVintage))

            } catch {
                // add code later
            }
        }
    }

    func fetchVintageByRegionAndWineType(regionName: String?, wineType: String, completion:@escaping(_ result:[String]) -> Void) {
        let request:NSFetchRequest<Wine> = Wine.fetchRequest()
        var format = "wine_color.color_name == %@"
        var predicate = NSPredicate(format:format, wineType)
        var wineVintage = Set<String>()
        if regionName != nil && regionName != "" {
            format = "wine_region.region_name == %@ AND wine_color.color_name == %@"
            predicate = NSPredicate(format:format, regionName!, wineType)
        }

        request.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try request.execute()
                print(result)

                for wine in result {

                    if wine.wine_vintage != 1111 {
                        wineVintage.insert(String(describing:wine.wine_vintage))
                    }
                }

                completion(Array(wineVintage))
            } catch {
                print(error)
            }
        }
    }
}
