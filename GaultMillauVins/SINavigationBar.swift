//
//  SINavigationBar.swift
//  SIBase
//
//  Created by Angel Antonov on 4/18/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation
import UIKit

enum SINavigationBarBackButtonType {
    case uiBarButtonItem, backIndicator, none
}

struct SINavigationBarBackButtonOptions {
    var backImageName: String
    var backButtonType: SINavigationBarBackButtonType
    
    init(backImageName: String = "back_btn".imageName, backButtonType: SINavigationBarBackButtonType = .uiBarButtonItem) {
        self.backImageName = backImageName
        self.backButtonType = backButtonType
    }
}

protocol SINavigationBar {
    func barWithTint(_ color: UIColor, translucent: Bool, backButtonOptions: SINavigationBarBackButtonOptions)
    func barWithBackground(_ imageName: String, translucent: Bool, backButtonOptions: SINavigationBarBackButtonOptions)
}

extension SINavigationBar where Self: UIViewController {
    func barWithTint(_ color: UIColor = rgb(255.0, 100.0, 100.0), translucent: Bool = false, backButtonOptions: SINavigationBarBackButtonOptions = SINavigationBarBackButtonOptions()) {
        self.navigationController?.navigationBar.isTranslucent = translucent
        self.navigationController?.navigationBar.barTintColor = color
        
        self.setupBackButton(backButtonOptions)
    }
    
    func barWithBackground(_ imageName: String = "topbar_nude".imageName, translucent: Bool = false, backButtonOptions: SINavigationBarBackButtonOptions = SINavigationBarBackButtonOptions()) {
        self.navigationController?.navigationBar.isTranslucent = translucent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: imageName)!.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 0), resizingMode: .stretch), for: .default)
        
        self.setupBackButton(backButtonOptions)
        
        self.navigationController?.navigationBar.tintColor = rgb(0.0)
    }
    
    func setupBackButton(_ backButtonOptions: SINavigationBarBackButtonOptions = SINavigationBarBackButtonOptions()) {
        if backButtonOptions.backImageName.isEmpty == false {
            if backButtonOptions.backButtonType == .uiBarButtonItem {
                let btnBack = UIButton(type: .custom)
                btnBack.setImage(UIImage(named: backButtonOptions.backImageName), for: UIControl.State())
                btnBack.frame.size = sizeOfImage(backButtonOptions.backImageName)
                btnBack.addTarget(self, action: #selector(UIViewController.backButtonTapped), for: .touchUpInside)
                
                let barBtnBack = UIBarButtonItem(customView: btnBack)
                
                self.navigationItem.leftBarButtonItems = [barBtnBack]
            }
            else if backButtonOptions.backButtonType == .backIndicator {
                let imgBackIndicator = UIImage(named: backButtonOptions.backImageName)!//.imageWithAlignmentRectInsets(UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0))
                
                self.navigationController?.navigationBar.backIndicatorImage = imgBackIndicator
                self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBackIndicator
            }
            else if backButtonOptions.backButtonType == .none {
                self.navigationController?.navigationBar.backIndicatorImage = nil
                self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = nil
                
                self.navigationItem.leftBarButtonItems = []
            }
        }
        else if backButtonOptions.backButtonType == .none {
            self.navigationController?.navigationBar.backIndicatorImage = nil
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = nil
            
            self.navigationItem.leftBarButtonItems = []
        }
    }
}

extension UIViewController {
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}
