//
//  Met+CoreDataProperties.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/4/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import Foundation
import CoreData


extension Met {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Met> {
        return NSFetchRequest<Met>(entityName: DbConstants.MetEntityName);
    }

    @NSManaged public var met_description: String
    @NSManaged public var met_name: String
    @NSManaged public var met_wine: NSSet

}

// MARK: Generated accessors for met_wine
extension Met {

    @objc(addMet_wineObject:)
    @NSManaged public func addToMet_wine(_ value: Wine)

    @objc(removeMet_wineObject:)
    @NSManaged public func removeFromMet_wine(_ value: Wine)

    @objc(addMet_wine:)
    @NSManaged public func addToMet_wine(_ values: NSSet)

    @objc(removeMet_wine:)
    @NSManaged public func removeFromMet_wine(_ values: NSSet)

}
