//
//  WineForAppelationListTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/24/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class WineForAppelationListTableViewController: UITableViewController, SILoadingViewRenderer {
    var wineAppelation = String()
    var metName = String()
    var dataSource = [Wine]()
    var wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 0.0, right: 0.0)
        DataManager.shared.fetchWinesByAppelation(wineAppelation: wineAppelation) {
            (result:[Wine]) in
            self.dataSource = result
            self.hideLoadingView()
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3 + dataSource.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.TitleCellReuseIdentifier, for: indexPath)
            cell.textLabel?.text = LocalizationConstants.MealWinesScreenTitleText
            let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0)
            let lineSeparator = UIView(frame: frame)
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.MealAppellationCellIdentifier, for: indexPath)
            cell.textLabel?.text = "\(metName), \(wineAppelation)"
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.DetailsCellIdentifier, for: indexPath)
            cell.textLabel?.text = LocalizationConstants.NameOfWineText
            cell.detailTextLabel?.text = LocalizationConstants.NoteText
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.WineByAppelationTableViewCell, for: indexPath) as! WineByAppelationTableViewCell
        if(dataSource[indexPath.row - 3].wine_vintage != 1111) {
            cell.wineNameLabel.text = "\(dataSource[indexPath.row - 3].wine_brand!) (\(dataSource[indexPath.row - 3].wine_vintage))"
        } else {
             cell.wineNameLabel.text = "\(dataSource[indexPath.row - 3].wine_brand!)"
        }
        let value = Double(dataSource[indexPath.row - 3].wine_note)
        cell.wineNoteLabel.text = String.localizedStringWithFormat("%.1f", value!)
        let frame = CGRect(x: cell.frame.origin.x + 20, y: cell.frame.size.height - 1.0, width: GlobalConstants.ScreenWidth - 40, height: 1.0)
        let lineSeparator = UIView(frame: frame)
        lineSeparator.backgroundColor = GlobalConstants.RedColor
        cell.addSubview(lineSeparator)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        wine = dataSource[indexPath.row - 3]
        performSegue(withIdentifier: GlobalConstants.ShowWineDeatilsForMealSegueName, sender: self)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return GlobalConstants.TitleCellHeight
        } else if indexPath.row == 2 {
            return GlobalConstants.StandartCellHeight
        } else {
            return GlobalConstants.MealWinesCellHeight
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.ShowWineDeatilsForMealSegueName {
            let destination = segue.destination as! WineDetailsForMealViewController
            destination.wine = wine
            destination.wineAppelaton = wineAppelation
        }
    }
}
