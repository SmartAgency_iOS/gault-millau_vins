//
//  VintageRatingEnum.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/29/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

enum VintageRatingEnum:Int {
    case PoorYear = 1, AverageYear, GoodYear, VeryGoodYear, Exceptional
    var text:String {
        switch self {
            case .PoorYear: return LocalizationConstants.PoorYear
            case .AverageYear: return LocalizationConstants.AverageYear
            case .GoodYear: return LocalizationConstants.GoodYear
            case .VeryGoodYear: return LocalizationConstants.VeryGoodYear
            case .Exceptional: return LocalizationConstants.Exceptional
        }
    }
    
}
