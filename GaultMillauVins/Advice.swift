//
//  Advice.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/14/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

struct Advice {
    public var name:String
    public var description:String
    
    // Decode
    public init(dictionary:Dictionary<String, String>) {
        name = dictionary[DbConstants.AdviceNameKey] as String!
        description = dictionary[DbConstants.AdviceDescriptionKey] as String!
    }
    
    // Encode
    public func encode() -> NSData {
        let dictionary = encodeToDict()
        let data:NSData = NSKeyedArchiver.archivedData(withRootObject: dictionary) as NSData
        return data
    }
    
    private func encodeToDict() -> Dictionary<String, String> {
        var dictionary : Dictionary = Dictionary<String, String>()
        dictionary[DbConstants.AdviceNameKey] = name as String!
        dictionary[DbConstants.AdviceDescriptionKey] = description as String!
        return dictionary
    }

}
