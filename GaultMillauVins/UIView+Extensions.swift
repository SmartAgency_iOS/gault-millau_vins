//
//  UIView+Extensions.swift
//  SIBase
//
//  Created by Angel Antonov on 11/10/15.
//  Copyright © 2015 SmartInteractive. All rights reserved.
//

import UIKit

extension UIView {
    var bottomYPoint: CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
    var mostRightPoint: CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    
    var lastViewBottomYPoint: CGFloat {
        if self.subviews.count == 0 {
            return 0
        }
        
        return self.subviews[self.subviews.count - 1].bottomYPoint
    }
    
    var lastViewМostRightPoint: CGFloat {
        if self.subviews.count == 0 {
            return 0
        }
        
        return self.subviews[self.subviews.count - 1].mostRightPoint
    }
    
    var blurredView: UIView {
        let blurEffect = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.dark))
        
        blurEffect.frame = self.frame
        blurEffect.alpha = 0.9
        
        return blurEffect
    }
    
    func bottomShadow() {
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2.0
    }
    
    func updateHeightTo(_ newHeight: CGFloat) {
        self.frame.size.height = newHeight
    }
    
    func updateWidthtTo(_ newWidth: CGFloat) {
        self.frame.size.width = newWidth
    }
    
    func updateHeightWith(_ additionalHeight: CGFloat) {
        self.frame.size.height += additionalHeight
    }
    
    func updateWidthWith(_ additionalWidth: CGFloat) {
        self.frame.size.width += additionalWidth
    }
    
    func updateXTo(_ newX: CGFloat) {
        self.frame.origin.x = newX
    }
    
    func updateYTo(_ newY: CGFloat) {
        self.frame.origin.y = newY
    }
    
    func updateXWith(_ additionalX: CGFloat) {
        self.frame.origin.x += additionalX
    }
    
    func updateYWith(_ additionalY: CGFloat) {
        self.frame.origin.y += additionalY
    }
    
    
    
    func printFrame() {
        NSLog("\nx \t\t- %f\ny \t\t- %f\nwidth \t- %f\nheight \t- %f", self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    }
    
    func printFrameWithName(_ name: String) {
        NSLog("%@\nx \t\t- %f\ny \t\t- %f\nwidth \t- %f\nheight \t- %f", name, self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    }
    
    func drawFrame() {
        self.drawFrameWithName("")
    }
    
    func drawFrameWithName(_ name: String) {
        NSLog("\nx - %f\ny - %f\n-----\n|   |\n|   |%@\n|   |%f\n-----\n  %f", self.frame.origin.x, self.frame.origin.y, name, self.frame.size.height, self.frame.size.width);
    }
    
    func centerWithY(_ newY: CGFloat, holderWidth: CGFloat) {
        self.frame = CGRect(x: (holderWidth - self.frame.size.width) / 2, y: newY, width: self.frame.size.width, height: self.frame.size.height);
    }
    
    func centerInView(_ view: UIView) {
        self.frame = CGRect(x: (view.frame.size.width - self.frame.size.width) / 2, y: (view.frame.size.height - self.frame.size.height) / 2, width: self.frame.size.width, height: self.frame.size.height);
    }
    
    func lastView() -> UIView? {
        if (self.subviews.count == 0) {
            return nil
        }
    
        return self.subviews[self.subviews.count - 1];
    }
    
    func removeAllSubviews() {
        for i in (0...self.subviews.count).reversed() {
            let v = self.subviews[i]
            v.removeFromSuperview()
        }
    }
    
    
    func hideSubviews() {
        for v in self.subviews {
            v.isHidden = true
        }
    }
    
    func unhideSubviews() {
        self.showSubviews()
    }
    
    func showSubviews() {
        for v in self.subviews {
            v.isHidden = false
        }
    }
    
    func rotate360Degrees(_ duration: CFTimeInterval = 1.0, completionDelegate: CAAnimationDelegate? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotateAnimation.fromValue = degreesToRadians(-90.0)
        rotateAnimation.toValue = degreesToRadians(270.0)
        rotateAnimation.duration = duration
        
        if let delegate: CAAnimationDelegate = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
    class func roundedView(_ withFrame: CGRect) -> UIView {
        let viewHolder = UIView(frame: withFrame)
        viewHolder.backgroundColor = UIColor.white
        viewHolder.layer.cornerRadius = 10.0
        viewHolder.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        viewHolder.layer.shadowColor = UIColor.black.cgColor
        viewHolder.layer.shadowOpacity = 0.2
        viewHolder.layer.shadowRadius = 2.0
        
        return viewHolder
    }
    
    func roundCorners() {
        self.layer.cornerRadius = 10.0
    }
    
    func shadowed() {
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2.0
    }
    
    func degreesToRadians(_ degrees: CGFloat) -> CGFloat {
        return (degrees + 90.0) * CGFloat.pi / 180.0
    }
    
    func radiansToDegrees(_ radians: CGFloat) -> CGFloat {
        return radians * 180.0 / CGFloat.pi
    }
}
