//
//  Wine+CoreDataProperties.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/4/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import Foundation
import CoreData


extension Wine {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Wine> {
        return NSFetchRequest<Wine>(entityName: DbConstants.WineEntityName);
    }

    @NSManaged public var wine_appelation: String?
    @NSManaged public var wine_brand: String?
    @NSManaged public var wine_classification: String?
    @NSManaged public var wine_cuvee: String?
    @NSManaged public var wine_note: String
    @NSManaged public var wine_price: NSDecimalNumber?
    @NSManaged public var wine_text: String
    @NSManaged public var wine_vintage: Int32
    @NSManaged public var wine_color: Color?
    @NSManaged public var wine_domain: Domain?
    @NSManaged public var wine_met: NSSet?
    @NSManaged public var wine_region: Region?

}

// MARK: Generated accessors for wine_met
extension Wine {

    @objc(addWine_metObject:)
    @NSManaged public func addToWine_met(_ value: Met)

    @objc(removeWine_metObject:)
    @NSManaged public func removeFromWine_met(_ value: Met)

    @objc(addWine_met:)
    @NSManaged public func addToWine_met(_ values: NSSet)

    @objc(removeWine_met:)
    @NSManaged public func removeFromWine_met(_ values: NSSet)

}
