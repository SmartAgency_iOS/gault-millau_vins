//
//  TutorialViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 1/3/17.
//  Copyright © 2017 Veronika Velkova. All rights reserved.
//

import UIKit

class TutorialViewController: UIPageViewController {
    var arrayOfImages = [UIImage]()
    var arrayOfTitles = [String]()
    var arrayOfDescriptions = [String]()
    weak var tutorialDelegate: TutorialPageViewControllerDelegate?

    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.showViewController(index: 0),
                self.showViewController(index: 1),
                self.showViewController(index: 2),
                self.showViewController(index: 3)]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        arrayOfImages.append(UIImage(named: "tutorial_first_page")!)
        arrayOfImages.append(UIImage(named: "tutorial_second_page")!)
        arrayOfImages.append(UIImage(named: "tutorial_third_page")!)
        arrayOfImages.append(UIImage(named: "tutorial_fourth_page")!)

        arrayOfTitles.append(LocalizationConstants.TutorialFirstTitle)
        arrayOfTitles.append(LocalizationConstants.TutorialSecondTitle)
        arrayOfTitles.append(LocalizationConstants.TutorialThirdTitle)
        arrayOfTitles.append(LocalizationConstants.TutorialFourthTitle)

        arrayOfDescriptions.append(LocalizationConstants.TutorialFirstDescription)
        arrayOfDescriptions.append(LocalizationConstants.TutorialSecondDescription)
        arrayOfDescriptions.append(LocalizationConstants.TutorialThirdPage)
        arrayOfDescriptions.append(LocalizationConstants.TutorialFourthDescription)

        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }

    private func showViewController(index:Int) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TutorialComponent") as! TutorialComponentViewController
        let viewToBack = viewController.view
        viewController.titleLabel.text = arrayOfTitles[index]
        viewController.descriptionLabel.text = arrayOfDescriptions[index]
        viewController.backgroundImage.image = arrayOfImages[index]
        self.view.sendSubviewToBack(viewToBack!)

        return viewController
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        dataSource = self
    }

}

extension TutorialViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1

        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }

        guard orderedViewControllers.count > previousIndex else {
            return nil
        }

        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }

        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count

        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }

        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

extension TutorialViewController: UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of: firstViewController) {
            tutorialDelegate?.tutorialPageViewController(tutorialPageViewController: self, didUpdatePageIndex: index)
        }
    }

}

protocol TutorialPageViewControllerDelegate: class {

    /**
     Called when the number of pages is updated.

     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter count: the total number of pages.
     */
    func tutorialPageViewController(tutorialPageViewController: TutorialViewController,
                                    didUpdatePageCount count: Int)

    /**
     Called when the current index is updated.

     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func tutorialPageViewController(tutorialPageViewController: TutorialViewController,
                                    didUpdatePageIndex index: Int)
    
}
