//
//  DomainPointAnnotation.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/30/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import MapKit

class DomainPointAnnotation: MKPointAnnotation {
    var image:String!
}
