//
//  TutorialComponentViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 1/3/17.
//  Copyright © 2017 Veronika Velkova. All rights reserved.
//

import UIKit

class TutorialComponentViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.backgroundColor = UIColor.clear
        descriptionLabel.backgroundColor = UIColor.clear
        skipButton.setTitle(LocalizationConstants.TutorialSkipButton, for: .normal)
        let line = UIView(frame: CGRect(x: self.view.frame.origin.x + 64, y: (self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 15), width: GlobalConstants.ScreenWidth - 128, height: 5))
        line.backgroundColor = GlobalConstants.RedColor
        let transparentView = UIView(frame: self.view.frame)
        transparentView.backgroundColor = UIColor.black
        transparentView.alpha = 0.8
        self.view.addSubview(transparentView)
        self.view.bringSubviewToFront(descriptionLabel)
        self.view.bringSubviewToFront(skipButton)
        self.view.bringSubviewToFront(titleLabel)
        self.view.insertSubview(line, aboveSubview: transparentView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func skipTutorial(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "skipTutorial"), object: nil)
    }
}
