//
//  DomainAnnotationView.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/30/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import MapKit

class DomainAnnotationView: UIView {
    @IBOutlet weak var domainNameLabel: UILabel!
    @IBOutlet weak var domainAddressLabel: UILabel!
    @IBOutlet weak var domainPostalCodeAndCityLabel: UILabel!
    @IBOutlet weak var domainPhoneLabel: UIButton!
    
    @IBAction func callDomain(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callDomain"), object: nil)
    }
}
