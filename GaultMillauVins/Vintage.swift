//
//  Vintage.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/11/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

struct VintageDataModel {
    public var name:String
    public var ratingForYear:[Int: Int]
    
    // Decode
    public init(dictionary:Dictionary<String, Any>) {
        name = (dictionary[DbConstants.VintageNameKey] as? String)!
        ratingForYear = (dictionary[DbConstants.RatingForYearKey] as? [Int: Int])!
    }
    
    // Encode
    public func encode() -> NSData {
        let dictionary = encodeToDict()
        let data:NSData = NSKeyedArchiver.archivedData(withRootObject: dictionary) as NSData
        return data
    }
    
    private func encodeToDict() -> Dictionary<String, AnyObject> {
        var dictionary : Dictionary = Dictionary<String, AnyObject>()
        dictionary[DbConstants.VintageNameKey] = name as String as AnyObject?
        dictionary[DbConstants.RatingForYearKey] = ratingForYear as [Int:Int] as AnyObject?
        return dictionary
    }
}


