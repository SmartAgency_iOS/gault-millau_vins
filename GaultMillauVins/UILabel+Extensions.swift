//
//  UILabel+Extensions.swift
//  SIBase
//
//  Created by iHustle on 2/17/16.
//  Copyright © 2016 iHome. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func adjustHeight() {
        self.numberOfLines = 0
        let newSize = self.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        
        self.frame.size.height = newSize.height
    }
    
    func adjustWidth() {
        self.numberOfLines = 0
        let newSize = self.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: self.frame.size.height))
        
        self.frame.size.width = newSize.width
    }
    
    var neededWidth: CGFloat {
        let currentNumberOfLines = self.numberOfLines
        
        self.numberOfLines = 0
        let neededSize = self.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: self.frame.size.height))
        self.numberOfLines = currentNumberOfLines
        
        return neededSize.width
    }
    
    var neededHeight: CGFloat {
        let currentNumberOfLines = self.numberOfLines
        
        self.numberOfLines = 0
        let neededSize = self.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        self.numberOfLines = currentNumberOfLines
        
        return neededSize.height
    }
}
