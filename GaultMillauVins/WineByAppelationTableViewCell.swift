//
//  WineByAppelationTableViewCell.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/25/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class WineByAppelationTableViewCell: UITableViewCell {
    @IBOutlet weak var wineNameLabel: UILabel!
    @IBOutlet weak var wineNoteLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
