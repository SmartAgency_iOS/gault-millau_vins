//
//  VintageInformationTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/29/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class VintageInformationTableViewController: UITableViewController {
    var dataSourceForRegionAsDictionary = Dictionary<Int, Int>()
    var dataSourceForYearAsDictionary = Dictionary<String,Int>()
    var isByRegionCriteria = Bool()
    var keyword = String()
    var keysForRegion = [Int]()
    var keysForYear = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keysForRegion = Array(dataSourceForRegionAsDictionary.keys).sorted().reversed()
        keysForYear = Array(dataSourceForYearAsDictionary.keys).sorted()
        let backButtonImage = UIImageView(image: UIImage(named:GlobalConstants.BackButtonImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        backButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size: backButtonImage.frame.size)
        self.navigationController?.view.addSubview(backButtonImage)
        self.tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 0.0, right: 0.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isByRegionCriteria) {
            return dataSourceForRegionAsDictionary.keys.count + 2
        }
        
        return dataSourceForYearAsDictionary.keys.count + 2
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier:GlobalConstants.TitleCellReuseIdentifier, for: indexPath)
            cell.textLabel?.text = LocalizationConstants.MillesimeTitleLabelText
            let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0)
            let lineSeparator = UIView(frame: frame)
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.KeywordCellIdentifier, for: indexPath)
            cell.textLabel?.text = keyword
            return cell
        }
        
        if isByRegionCriteria {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.VintageByRegionCellIdentifier, for: indexPath) as! VintageByRegionTableViewCell
            
            let key = keysForRegion[indexPath.row - 2]
            cell.yearLabel.text = String(key)
            for image in cell.rate {
                if(cell.rate.index(of: image)! < dataSourceForRegionAsDictionary[key]!) {
                    image.image = UIImage(named: GlobalConstants.FullGlassImageName)
                } else {
                    image.image = UIImage(named: GlobalConstants.EmptyGlassImageName)
                }
            }
            cell.ratingTextLabel.text = VintageRatingEnum(rawValue: dataSourceForRegionAsDictionary[key]!)!.text
            let frame = CGRect(x: cell.frame.origin.x + 20, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth - 40, height: 1.0)
            let lineSeparator = UIView(frame:frame)
            lineSeparator.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(lineSeparator)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.VintageByYearCellIdentifier, for: indexPath) as! VintageByYearTableViewCell
        let key = keysForYear[indexPath.row - 2]
        cell.regionNameLabel.text = key
        for image in cell.rate {
            if(cell.rate.index(of: image)! < dataSourceForYearAsDictionary[key]!) {
                image.image = UIImage(named: GlobalConstants.FullGlassImageName)
            } else {
                image.image = UIImage(named: GlobalConstants.EmptyGlassImageName)
            }
        }
        cell.ratingTextLabel.text = VintageRatingEnum(rawValue: dataSourceForYearAsDictionary[key]!)!.text
        let frame = CGRect(x: cell.frame.origin.x + 20, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth - 40, height: 1.0)
        let lineSeparator = UIView(frame:frame)
        lineSeparator.backgroundColor = GlobalConstants.RedColor
        cell.addSubview(lineSeparator)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return GlobalConstants.TitleCellHeight
        } else if indexPath.row == 1 {
            return GlobalConstants.StandartCellHeight
        }
        
        if !isByRegionCriteria {
            return 104.0
        }
        
        return 64.0
    }
}
