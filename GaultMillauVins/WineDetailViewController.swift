//
//  WineDetailViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/17/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import Branch

class WineDetailViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var wineDescriptionLabel: UILabel!
    @IBOutlet weak var domainNameLabel: UILabel!
    @IBOutlet weak var regionNameLabel: UILabel!
    @IBOutlet weak var wineAppelationLabel: UILabel!
    @IBOutlet weak var wineNoteLabel: UILabel!
    @IBOutlet weak var winePriceLabel: UILabel!
    @IBOutlet weak var wineTextLabel: UILabel!
    @IBOutlet weak var metsVinsLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var appelationLabel: UILabel!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var mealsWinesTitleLabel: UILabel!
    @IBOutlet weak var seeOnMapButton: UIButton!
    @IBOutlet weak var callDomainButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var scrollViewContainer: UIView!
    
    var wine:Wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    var favourites:[WineDataModel] = []
    let point = DomainPointAnnotation()
    var pinAnnotationView:MKPinAnnotationView!
    var location:String!
    var mapView:MKMapView?
    var shareButtonImage:UIImageView!


    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.layoutIfNeeded()
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
        } else {
            favourites = [WineDataModel]()
        }
        
        let backButtonImage = UIImageView(image: UIImage(named:GlobalConstants.BackButtonImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        backButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size: backButtonImage.frame.size)
        self.navigationController?.view.addSubview(backButtonImage)

        let shareButton = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(displayShareSheet(sender:)))
        self.navigationItem.rightBarButtonItem = shareButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUI()

        shareButtonImage = UIImageView(image: UIImage(named: "share")?.withRenderingMode(.alwaysOriginal))
        shareButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.size.width)! - 50, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size:  shareButtonImage.frame.size)
        self.navigationController?.view.addSubview(shareButtonImage)
    }

    @objc func displayShareSheet(sender:UIBarButtonItem) {
        let uri = wine.objectID.uriRepresentation()
        let deepLinkString = "wine://wine/\(uri)"

        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "wine/\(uri)")
        branchUniversalObject.title = LocalizationConstants.FacebookShareTitle
        branchUniversalObject.contentDescription = LocalizationConstants.ShareSingleWineDescription
        branchUniversalObject.addMetadataKey("id", value: "\(uri)")

        print(uri)
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        linkProperties.channel = "facebook"
        linkProperties.addControlParam("$ios_url", withValue: deepLinkString)
        linkProperties.addControlParam("$email_subject", withValue: LocalizationConstants.MailSubject)

        branchUniversalObject.showShareSheet(with: linkProperties,
                                             andShareText: LocalizationConstants.ShareSingleWineDescription,
                                             from: self) {
                                                (activityType, completed) in
                                                NSLog("done showing share sheet!")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

        self.mapViewContainer.translatesAutoresizingMaskIntoConstraints = false
        mapView = MKMapView(frame: CGRect(x: mapViewContainer.frame.origin.x, y: mapViewContainer.frame.origin.y, width: mapViewContainer.frame.size.width, height: mapViewContainer.frame.size.height))
        self.scrollViewContainer.addSubview(mapView!)
        mapView!.mapType = .standard
        mapView!.delegate = self
        let coordinates:CLLocationCoordinate2D!

        if (wine.wine_domain!.domain_address?.address_latitude.isEqual(to: 0.000000))! {
            coordinates = CLLocationCoordinate2DMake(46.60, 2.26)
            self.mapView!.centerCoordinate = coordinates
        } else {
            coordinates = CLLocationCoordinate2DMake((wine.wine_domain!.domain_address!.address_latitude), (wine.wine_domain!.domain_address!.address_longitute))
            self.mapView!.centerCoordinate = coordinates
            self.point.image = GlobalConstants.WineDetailsPinImageName
            self.point.coordinate = coordinates
            self.pinAnnotationView = MKPinAnnotationView(annotation: self.point, reuseIdentifier: GlobalConstants.AnnotationViewReuseIdentifier)
            self.mapView!.addAnnotation(self.pinAnnotationView.annotation!)
        }

        self.mapView?.setRegion(MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 1.5, longitudeDelta: 1.5)), animated: true)
    }


    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        shareButtonImage.removeFromSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        mapView?.removeFromSuperview()
        mapView?.delegate = nil

    }

    deinit {
        print("DEINIT")
        mapView?.mapType = .hybrid
        mapView?.removeAnnotations((self.mapView?.annotations)!)
        mapView?.removeFromSuperview()
        mapView?.delegate = nil
        mapView = nil
    }
    
    func setupUI() {

        wineDescriptionLabel.sizeToFit()
        regionLabel.text = LocalizationConstants.RegionLabelText
        appelationLabel.text = LocalizationConstants.AppellationLabelText
        descriptionTitleLabel.text = LocalizationConstants.WineDescriptionTitleLabelText
        mealsWinesTitleLabel.text = LocalizationConstants.MealsWineTitleLabelText
        seeOnMapButton.setTitle(LocalizationConstants.SeeOnMapButtonText, for: .normal)
        callDomainButton.setTitle(LocalizationConstants.CallDomainButtonText, for: .normal)
        
        favouriteButton.setImage(UIImage(named:GlobalConstants.FavouriteButtonIconImageName), for: .normal)
        let wineData = WineDataModel(wineAppeleation: wine.wine_appelation ?? "", wineCuvee: wine.wine_cuvee ?? "", wineBrand: wine.wine_brand ?? "", wineClassification: wine.wine_classification ?? "", wineNote:wine.wine_note, winePrice: (wine.wine_price ?? 0) as NSDecimalNumber, wineText: wine.wine_text, wineVintage: wine.wine_vintage, wineDomainName: wine.wine_domain!.domain_name!, wineRegion: wine.wine_region!.region_name , wineColor: wine.wine_color!.color_name)
        for object in favourites {
            if checkIfEqual(object: object, wineData: wineData) {
                favouriteButton.setImage(UIImage(named:GlobalConstants.UnfavouriteButtonIconImageName), for: .normal)
                break
            }
        }
        if wine.wine_vintage != 1111 {
        wineDescriptionLabel.text = String(format: "\(wine.wine_brand!) \(wine.wine_classification!) \(wine.wine_cuvee!) \(wine.wine_vintage), \(wine.wine_color!.color_name)")
        } else {
            wineDescriptionLabel.text = String(format: "\(wine.wine_brand!) \(wine.wine_classification!) \(wine.wine_cuvee!), \(wine.wine_color!.color_name)")
        }
        domainNameLabel.text = wine.wine_domain!.domain_name
        regionNameLabel.text = wine.wine_region!.region_name
        wineAppelationLabel.text = wine.wine_appelation
        let value = Double(wine.wine_note)
        wineNoteLabel.text = String.localizedStringWithFormat("%.1f", value!)
        if wine.wine_price != 0 {
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = NumberFormatter.Style.currency
            currencyFormatter.locale = NSLocale.current
            let priceString = currencyFormatter.string(from: wine.wine_price!)
            winePriceLabel.text = priceString
        } else {
            winePriceLabel.text = GlobalConstants.PriceNotAvailableText
        }

        wineTextLabel.text = wine.wine_text
        var text = String()
        
        DataManager.shared.fetchMealsForWine(wine: wine) {
            (result:[String]) in
            for name in result {
                text.append("\(name) ")
            }
            self.metsVinsLabel.text = text
        }
    }
    
    func checkIfEqual(object:WineDataModel, wineData:WineDataModel)->(Bool) {
        return object.wineAppeleation == wineData.wineAppeleation && object.wineCuvee == wineData.wineCuvee && object.wineBrand == wineData.wineBrand && object.wineClassification == wineData.wineClassification && object.wineNote == wineData.wineNote && object.winePrice == wineData.winePrice && object.wineText == wineData.wineText
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = GlobalConstants.AnnotationViewReuseIdentifier
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        let customPointAnnotation = annotation as! DomainPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.image)
        
        return annotationView
    }
    
    @IBAction func findAddressOnMap(_ sender: Any) {
    }
    
    @IBAction func callDomain(_ sender: Any) {
        let phone = wine.wine_domain!.domain_phone?.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://\(phone!)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, completionHandler:nil)
            print(phone!)
        }
    }
        
    @IBAction func makeFavourite(_ sender: Any) {
        let count = favourites.count
        print(wine)
        let wineData = WineDataModel(wineAppeleation: wine.wine_appelation ?? "", wineCuvee: wine.wine_cuvee ?? "", wineBrand: wine.wine_brand ?? "", wineClassification: wine.wine_classification ?? "", wineNote:wine.wine_note, winePrice: (wine.wine_price ?? 0) as NSDecimalNumber, wineText: wine.wine_text, wineVintage: wine.wine_vintage, wineDomainName: wine.wine_domain!.domain_name!, wineRegion: wine.wine_region!.region_name , wineColor: wine.wine_color!.color_name)
        for object in favourites {
            if checkIfEqual(object: object, wineData: wineData) {
                if let index = favourites.index(of: object) {
                    favourites.remove(at: index)
                    favouriteButton.setImage(UIImage(named:GlobalConstants.FavouriteButtonIconImageName), for: .normal)
                    break
                }
            }
        }
        
        if(count == favourites.count) {
            favourites.append(wineData)
            favouriteButton.setImage(UIImage(named:GlobalConstants.UnfavouriteButtonIconImageName), for: .normal)
        }
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: favourites as NSArray)
        UserDefaults.standard.set(encodedData, forKey:GlobalConstants.FavouriteWinesKey)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.AddedNewFavouriteWineNotificationName), object: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.ShowLocationOnMapSegueIdentifier {
            let destination = segue.destination as! DetailedMapViewController
            destination.wine = wine
        }
    }
}
