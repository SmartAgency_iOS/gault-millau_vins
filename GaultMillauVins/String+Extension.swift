//
//  String+Extension.swift
//  SIBase
//
//  Created by Angel Antonov on 5/28/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

import UIKit
import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    var imageName : String {
        if UIScreen.main.bounds.height == 667.0 { // if it's iPhone 6 get the correct image
            return String("\(self)_ip6")
        }
        
        return self
    }
    
    func toDate(_ format: String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = format
        
        return df.date(from: self)
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func contains(_ strText: String) -> Bool {
        return self.lowercased().range(of: strText.lowercased()) != nil
    }
    
    var log: Void {
        print(self)
    }
    
    func toDouble() -> Double? {
        let nf = NumberFormatter()
        nf.locale = Locale(identifier: "en-US")
        
        return nf.number(from: self)?.doubleValue
    }
    
    func underline(_ color: UIColor) -> NSMutableAttributedString {
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: self)
        attString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, self.characters.count))
        attString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, self.localized.characters.count))
        
        return attString
    }
    
    func insert(_ string:String,ind:Int) -> String {
        return  String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count-ind))
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func getDateWithFormat(_ stringFormat: String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = stringFormat
        df.timeZone = TimeZone.autoupdatingCurrent
        df.locale = Locale.current
        
        return df.date(from: self)
    }
}
