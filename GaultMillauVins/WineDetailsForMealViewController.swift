//
//  WineDetailsForMealViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/24/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import Branch

class WineDetailsForMealViewController: UIViewController {
    @IBOutlet weak var chosenAppellation: UILabel!
    @IBOutlet weak var wineDescriptionLabel: UILabel!
    @IBOutlet weak var domainNameLabel: UILabel!
    @IBOutlet weak var regionNameLabel: UILabel!
    @IBOutlet weak var wineAppelationLabel: UILabel!
    @IBOutlet weak var wineNoteLabel: UILabel!
    @IBOutlet weak var winePriceLabel: UILabel!
    @IBOutlet weak var wineTextLabel: UILabel!
    @IBOutlet weak var metsVinsLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var domainInfoLabel: UILabel!
    @IBOutlet weak var domainMailLabel: UILabel!
    @IBOutlet weak var domainWebSiteLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var appelationLabel: UILabel!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var mealsWinesTitleLabel: UILabel!
    @IBOutlet weak var seeOnMapButton: UIButton!
    @IBOutlet weak var callDomainButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    var wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    var favourites:[WineDataModel] = []
    var wineAppelaton:String!
    var shareButtonImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(wine)
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
        } else {
            favourites = [WineDataModel]()
        }

        let backButtonImage = UIImageView(image: UIImage(named:GlobalConstants.BackButtonImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        backButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 10, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size: backButtonImage.frame.size)
        self.navigationController?.view.addSubview(backButtonImage)

        let shareButton = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(displayShareSheet(sender:)))
        self.navigationItem.rightBarButtonItem = shareButton


        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        shareButtonImage = UIImageView(image: UIImage(named: "share")?.withRenderingMode(.alwaysOriginal))
        shareButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.size.width)! - 50, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size:  shareButtonImage.frame.size)
        self.navigationController?.view.addSubview(shareButtonImage)
    }

    @objc func displayShareSheet(sender:UIBarButtonItem) {
        let uri = wine.objectID.uriRepresentation()
        let deepLinkString = "wine://designation/\(uri)"

        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "designation/\(uri)")
        branchUniversalObject.title = LocalizationConstants.FacebookShareTitle
        branchUniversalObject.contentDescription = LocalizationConstants.ShareSingleWineDescription
        branchUniversalObject.addMetadataKey("id", value: "\(uri)")

        print(uri)
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        linkProperties.addControlParam("$ios_url", withValue: deepLinkString)
        linkProperties.addControlParam("$email_subject", withValue: LocalizationConstants.MailSubject)
        linkProperties.channel = "facebook"
        branchUniversalObject.showShareSheet(with: linkProperties,
                                             andShareText: LocalizationConstants.ShareSingleWineDescription,
                                             from: self) {
                                                (activityType, completed) in
                                                NSLog("done showing share sheet!")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        shareButtonImage.removeFromSuperview()
    }
    
    func setupUI() {
        scrollView.layoutIfNeeded()
        wineDescriptionLabel.sizeToFit()
        chosenAppellation.text = wine.wine_appelation
        regionLabel.text = LocalizationConstants.RegionLabelText
        appelationLabel.text = LocalizationConstants.AppellationLabelText
        descriptionTitleLabel.text = LocalizationConstants.WineDescriptionTitleLabelText
        mealsWinesTitleLabel.text = LocalizationConstants.MealsWineTitleLabelText
        seeOnMapButton.setTitle(LocalizationConstants.SeeOnMapButtonText, for: .normal)
        callDomainButton.setTitle(LocalizationConstants.CallDomainButtonText, for: .normal)
        
        favouriteButton.setImage(UIImage(named:GlobalConstants.FavouriteButtonIconImageName), for: .normal)
        let wineData = WineDataModel(wineAppeleation: wine.wine_appelation ?? "", wineCuvee: wine.wine_cuvee ?? "", wineBrand: wine.wine_brand ?? "", wineClassification: wine.wine_classification ?? "", wineNote:wine.wine_note, winePrice: (wine.wine_price ?? 0) as NSDecimalNumber, wineText: wine.wine_text, wineVintage: wine.wine_vintage, wineDomainName: wine.wine_domain!.domain_name!, wineRegion: wine.wine_region!.region_name , wineColor: wine.wine_color!.color_name)
        for object in favourites {
            if checkIfEqual(object: object, wineData: wineData) {
                favouriteButton.setImage(UIImage(named:GlobalConstants.UnfavouriteButtonIconImageName), for: .normal)
                break
            }
        }
        if wine.wine_vintage != 1111 {
            wineDescriptionLabel.text = "\(wine.wine_brand!) \(wine.wine_classification!) \(wine.wine_cuvee!) \(wine.wine_vintage), \(wine.wine_color!.color_name)"
        } else {
            wineDescriptionLabel.text = "\(wine.wine_brand!) \(wine.wine_classification!) \(wine.wine_cuvee!), \(wine.wine_color!.color_name)"
        }
        wineDescriptionLabel.sizeToFit()
        domainNameLabel.text = wine.wine_domain!.domain_name
        regionNameLabel.text = wine.wine_region!.region_name
        wineAppelationLabel.text = wine.wine_appelation
        let value = Double(wine.wine_note)
        wineNoteLabel.text = String.localizedStringWithFormat("%.1f", value!)
        if wine.wine_price != 0 {
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = NumberFormatter.Style.currency
            currencyFormatter.locale = NSLocale.current
            let priceString = currencyFormatter.string(from: wine.wine_price!)
            winePriceLabel.text = priceString
        } else {
            winePriceLabel.text = GlobalConstants.PriceNotAvailableText
        }
        
        wineTextLabel.text = wine.wine_text
        var text = String()
        
        DataManager.shared.fetchMealNamesForWine(wine: wine) {
            (result:[String]) in
            for name in result {
                text.append("\(name) \n")
            }
            self.metsVinsLabel.text = text
        }
        
        domainInfoLabel.text = "\(String(describing: self.wine.wine_domain!.domain_name)) \n\(String(describing: self.wine.wine_domain!.domain_address)) \n\(String(describing: self.wine.wine_domain!.domain_post_code)) \(String(describing: self.wine.wine_domain!.domain_city))"
        
        domainMailLabel.text = wine.wine_domain!.domain_email
        domainWebSiteLabel.text = wine.wine_domain!.domain_website
    }
    
    func checkIfEqual(object:WineDataModel, wineData:WineDataModel)->(Bool) {
        return object.wineAppeleation == wineData.wineAppeleation && object.wineCuvee == wineData.wineCuvee && object.wineBrand == wineData.wineBrand && object.wineClassification == wineData.wineClassification && object.wineNote == wineData.wineNote && object.winePrice == wineData.winePrice && object.wineText == wineData.wineText
    }
    
    @IBAction func findAddressOnMap(_ sender: Any) {
        performSegue(withIdentifier: GlobalConstants.ShowDomainOnMapSegueIdentifier, sender: self)
    }

    @IBAction func callDomain(_ sender: Any) {
        let phone = wine.wine_domain!.domain_phone!.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, completionHandler:nil)
            print(phone)
        }
    }

    @IBAction func makeFavourite(_ sender: Any) {
        let count = favourites.count
        print(wine)
        let wineData = WineDataModel(wineAppeleation: wine.wine_appelation ?? "", wineCuvee: wine.wine_cuvee ?? "", wineBrand: wine.wine_brand ?? "", wineClassification: wine.wine_classification ?? "", wineNote:wine.wine_note, winePrice: (wine.wine_price ?? 0) as NSDecimalNumber, wineText: wine.wine_text, wineVintage: wine.wine_vintage, wineDomainName: wine.wine_domain!.domain_name!, wineRegion: wine.wine_region!.region_name , wineColor: wine.wine_color!.color_name)
        for object in favourites {
            if checkIfEqual(object: object, wineData: wineData) {
                if let index = favourites.index(of: object) {
                    favourites.remove(at: index)
                    favouriteButton.setImage(UIImage(named:GlobalConstants.FavouriteButtonIconImageName), for: .normal)
                    break
                }
            }
        }
        
        if(count == favourites.count) {
            favourites.append(wineData)
            favouriteButton.setImage(UIImage(named:GlobalConstants.UnfavouriteButtonIconImageName), for: .normal)
        }
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: favourites as NSArray)
        UserDefaults.standard.set(encodedData, forKey:GlobalConstants.FavouriteWinesKey)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.AddedNewFavouriteWineNotificationName), object: nil)

    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.ShowDomainOnMapSegueIdentifier {
            let destination = segue.destination as! DetailedMapViewController
            destination.location = "\(wine.wine_domain!.domain_address) \(String(describing: wine.wine_domain!.domain_city))"
            destination.wine = wine
        }
    }
}
