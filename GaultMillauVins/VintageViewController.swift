//
//  VintageViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/23/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import DropDown

class VintageViewController: UIViewController, SINavigationBar {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var regionView: UIButton!
    @IBOutlet weak var vintageView: UIButton!
    @IBOutlet weak var regionArrow: UIImageView!
    @IBOutlet weak var vintageArrow: UIImageView!
    @IBOutlet weak var chooseRegionTextLabel: UILabel!
    @IBOutlet weak var chooseYearTextLabel: UILabel!
    
    let regionDropDown = DropDown()
    let vintageDropDown = DropDown()

    var regionCriteria:String?, vintageCriteria:String?
    var burgerImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barWithTint(UIColor.white, translucent: false, backButtonOptions: SINavigationBarBackButtonOptions(backButtonType: .none))
        self.setNavigationBarItem()
        
        DataManager.shared.populateVintageDataSource() {
            (completion: [String]) in
            self.vintageDropDown.dataSource = completion
        }
        
        DataManager.shared.populateRegionDataSourceForVintage() {
            (completion: [String]) in
            self.regionDropDown.dataSource = completion
        }
        
        setupDropDowns()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUI()
        let header = UIImageView(image: UIImage(named:GlobalConstants.HeaderMenuImageName))
        header.frame = CGRect(x: (self.navigationController?.navigationBar.frame.origin.x)!, y: (self.navigationController?.navigationBar.frame.origin.y)!, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)! + 20)
        self.navigationController?.view.addSubview(header)
        
        burgerImage = UIImageView(image: UIImage(named:GlobalConstants.BurgerMenuImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        burgerImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)!), size: burgerImage.frame.size)
        header.addSubview(burgerImage)
        
        regionCriteria = ""
        vintageCriteria = ""
    }
    
    func setupDropDowns() {
        regionDropDown.anchorView = regionView
        regionDropDown.bottomOffset = CGPoint(x: 0, y: regionView.bounds.height)
        regionDropDown.direction = .bottom
        regionDropDown.cellNib = UINib(nibName: GlobalConstants.DropDownCellNibName, bundle: Bundle(for: DropDownCell.self))
        regionDropDown.cellHeight = GlobalConstants.DropDownCellHeight
        regionDropDown.backgroundColor = UIColor.white
        regionDropDown.textColor = GlobalConstants.RedColor
        regionDropDown.customCellConfiguration =  { (index: Index, item: String, cell: DropDownCell) -> Void in
            let frame = CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: cell.frame.size.width - 60, height: 1)
            let line = UIView(frame: frame)
            line.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(line)
            cell.optionLabel.alignmentRect(forFrame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.origin.y - 10, width: 300, height: 44))
        }
        
        regionDropDown.selectionAction = { [unowned self] (index, item) in
            self.regionView.setTitle(item, for: .normal)
            self.regionCriteria = item
            self.regionView.backgroundColor = UIColor.white
            self.regionView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.regionArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
            self.vintageView.setTitle(LocalizationConstants.VintageListText, for: .normal)
            self.vintageCriteria = ""
            self.performSegue(withIdentifier: "ShowVintageInformation", sender: self)
        }
        
        regionDropDown.cancelAction = { [unowned self] in
            self.regionView.backgroundColor = UIColor.white
            self.regionView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.regionArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        
        vintageDropDown.anchorView = vintageView
        vintageDropDown.bottomOffset = CGPoint(x: 0, y: vintageView.bounds.height)
        vintageDropDown.direction = .bottom
        vintageDropDown.cellNib = UINib(nibName: GlobalConstants.DropDownCellNibName, bundle: Bundle(for: DropDownCell.self))
        vintageDropDown.customCellConfiguration = nil
        vintageDropDown.selectionAction = { [unowned self] (index, item) in
            self.vintageView.setTitle(item, for: .normal)
            self.vintageCriteria = item
            self.vintageView.backgroundColor = UIColor.white
            self.vintageView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.vintageArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
            self.regionView.setTitle(LocalizationConstants.RegionListText, for: .normal)
            self.regionCriteria = ""
            self.performSegue(withIdentifier: "ShowVintageInformation", sender: self)
        }
        
        vintageDropDown.cancelAction = { [unowned self] in
            self.vintageView.backgroundColor = UIColor.white
            self.vintageView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.vintageArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        
        vintageDropDown.cellHeight = GlobalConstants.DropDownCellHeight
        vintageDropDown.backgroundColor = UIColor.white
        vintageDropDown.textColor = GlobalConstants.RedColor
        vintageDropDown.customCellConfiguration =  { (index: Index, item: String, cell: DropDownCell) -> Void in
            let frame = CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width:  cell.frame.size.width - 60, height: 1)
            let line = UIView(frame: frame)
            line.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(line)
        }
    }
    
    func setupUI() {
        titleLabel.backgroundColor = UIColor.white
        titleLabel.text = LocalizationConstants.MillesimeTitleLabelText
        chooseRegionTextLabel.text = LocalizationConstants.ChooseRegionLabelText
        chooseYearTextLabel.text = LocalizationConstants.ChooseVintageLabelText
        let line = UIView(frame: CGRect(x: titleLabel.frame.origin.x, y: titleLabel.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0))
        line.backgroundColor =
            GlobalConstants.LightGrayColor
        titleLabel.addSubview(line)
        regionView.layer.borderColor = GlobalConstants.RedColorCgValue
        regionView.backgroundColor = UIColor.white
        regionView.layer.borderWidth = 2
        regionView.setTitle(LocalizationConstants.RegionListText, for: .normal)
        
        vintageView.layer.borderColor = GlobalConstants.RedColorCgValue
        vintageView.backgroundColor = UIColor.white
        vintageView.layer.borderWidth = 2
        vintageView.setTitle(LocalizationConstants.VintageListText, for: .normal)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func chooseRegion(_ sender: Any) {
        regionDropDown.show()
        
        if(!regionDropDown.isHidden) {
            regionView.backgroundColor = GlobalConstants.RedColor
            regionView.setTitleColor(UIColor.white, for: .normal)
            regionArrow.image = UIImage(named: GlobalConstants.TriangleDownWiteImageName)
        }
    }
    
    
    @IBAction func chooseVintage(_ sender: Any) {
        vintageDropDown.show()
        
        if(!vintageDropDown.isHidden) {
            vintageView.backgroundColor = GlobalConstants.RedColor
            vintageView.setTitleColor(UIColor.white, for: .normal)
            vintageArrow.image = UIImage(named: GlobalConstants.TriangleDownWiteImageName)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowVintageInformation" {
            let destinationVC = segue.destination as! VintageInformationTableViewController
            if(regionCriteria != "" && regionCriteria != nil) {
                DataManager.shared.fetchYearRatingsByRegion(regionCriteria: regionCriteria!) {
                    (completion:Dictionary<Int, Int>) in
                    destinationVC.dataSourceForRegionAsDictionary = completion
                    destinationVC.isByRegionCriteria = true
                    destinationVC.keyword = regionCriteria!
                    print(destinationVC.dataSourceForRegionAsDictionary)
                }
            } else if vintageCriteria != "" && vintageCriteria != nil {
                DataManager.shared.fetchRegionRatingsByYear(vintageCriteria: Int(vintageCriteria!)!) {
                    (completion:Dictionary<String, Int>) in
                    destinationVC.dataSourceForYearAsDictionary = completion
                    destinationVC.isByRegionCriteria = false
                    destinationVC.keyword = vintageCriteria!
                    print(destinationVC.dataSourceForYearAsDictionary)
                }
            }
        }
    }
}

