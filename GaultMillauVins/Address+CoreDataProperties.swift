//
//  Address+CoreDataProperties.swift
//  
//
//  Created by Veronika Velkova on 1/5/17.
//
//

import Foundation
import CoreData


extension Address {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address");
    }

    @NSManaged public var address_name: String?
    @NSManaged public var address_latitude: Double
    @NSManaged public var address_longitute: Double
    @NSManaged public var address_city: String?
    @NSManaged public var address_domain: Domain?

}
