//
//  DbConstants.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/8/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

struct DbConstants {
    // entity names
    static let MetEntityName = "Met"
    static let ColorEntityName = "Color"
    static let DomainEntityName = "Domain"
    static let VintageEntityName = "Vintage"
    static let RegionEntityName = "Region"
    static let WineEntityName = "Wine"
    static let AdviceEntityName = "Advice"
    
    // file names
    static let MetFileName = "ZMET.csv"
    static let MealWineFileName = "ZMETVIN.csv"
    static let VintageFileName = "ZMILLESIME.csv"
    static let WineFileName = "ZWINE.csv"
    static let AdviceFileName = "ZADVICE.csv"
    
    // encoding
    static let encoding = String.Encoding.utf8.rawValue
    
    // db context strings
    static let PersistentContainerName = "GaultMillauVins"
    static let ManagedObjectResourceName = "GaultMillauVins"
    static let ManagedObjectResourceExtension = "momd"
    static let PersistentStoreCoordinatorUrl = "GaultMillauVins.sqlite"
    
    // vintage data model
    static let VintageNameKey = "name"
    static let RatingForYearKey = "ratingForYear"
    
    // advice data model
    static let AdviceNameKey = "name"
    static let AdviceDescriptionKey = "description"
    
    // predicate
    static let DomainNameFormat = "domain_name == %@"
    static let RegionNameFormat = "region_name == %@"
    static let ColorNameFormat = "color_name == %@"
    static let WineAppelationFormat = "wine_appelation == %@"
    static let MetNameFormat = "met_name == %@"
}
