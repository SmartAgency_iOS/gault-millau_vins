//
//  BaseTableViewCell.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/17/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import DropDown

class BaseTableViewCell: UITableViewCell {
    @IBOutlet weak var winesCountLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var arrowImage: UIImageView!
    let dropDown = DropDown()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dropDown.anchorView = filterButton
        dropDown.dataSource = [LocalizationConstants.SortByIncreasingPrice, LocalizationConstants.SortBydecreasingPrice, LocalizationConstants.SortByIncreasingYear, LocalizationConstants.SortBydecreasingYear]
        
        dropDown.bottomOffset = CGPoint(x: 0, y: filterButton.bounds.height)
        dropDown.direction = .bottom
        dropDown.cellNib = UINib(nibName: GlobalConstants.DropDownCellNibName, bundle: Bundle(for: DropDownCell.self))
        dropDown.cellHeight = GlobalConstants.DropDownCellHeight
        dropDown.backgroundColor = UIColor.white
        dropDown.textColor = GlobalConstants.RedColor
        dropDown.textFont = UIFont.systemFont(ofSize: 11.0)
        dropDown.customCellConfiguration =  { (index: Index, item: String, cell: DropDownCell) -> Void in
            let frame = CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: cell.frame.size.width - 60, height: 1)
            let line = UIView(frame: frame)
            line.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(line)
            }
        
            dropDown.selectionAction = { [unowned self] (index, item) in
            self.filterButton.setTitle(item, for: .normal)
            self.filterButton.backgroundColor = UIColor.white
            self.filterButton.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.arrowImage.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)

            if(self.filterButton.title(for: .normal) == LocalizationConstants.SortByIncreasingPrice) {
                print("Increasing price")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.IncreasingPriceNotificationName), object: nil)
            } else if(self.filterButton.title(for: .normal) == LocalizationConstants.SortBydecreasingPrice) {
                print("Decreasing price")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.DecreasingPriceNotificationName), object: nil)
            } else if(self.filterButton.title(for: .normal) == LocalizationConstants.SortByIncreasingYear) {
                print("Increasing year")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.IncreasingVintageNotificationName), object: nil)
            } else if(self.filterButton.title(for: .normal) == LocalizationConstants.SortBydecreasingYear) {
                print("Decreasing year")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.DecreasingVintageNotificationName), object: nil)
            }
        }
        
        dropDown.cancelAction = { [unowned self] in
            self.filterButton.backgroundColor = UIColor.white
            self.filterButton.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.arrowImage.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        
        winesCountLabel.backgroundColor = UIColor.white
        filterButton.backgroundColor = UIColor.white
        filterButton.layer.borderWidth = 2.0
        filterButton.layer.borderColor = GlobalConstants.RedColorCgValue
        filterButton.setTitle(LocalizationConstants.SortResult, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func chooseFilter(_ sender: Any) {
        dropDown.show()
        
        if(!dropDown.isHidden) {
            filterButton.backgroundColor = GlobalConstants.RedColor
            filterButton.setTitleColor(UIColor.white, for: .normal)
            arrowImage.image = UIImage(named: GlobalConstants.TriangleDownWiteImageName)
        }

    }
}
