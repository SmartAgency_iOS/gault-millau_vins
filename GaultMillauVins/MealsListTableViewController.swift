//
//  MealsListTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/24/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class MealsListTableViewController: UITableViewController, SINavigationBar, SILoadingViewRenderer {
    var dataSource = [Met]()
    var met:Met = Met(context: DataBaseContext.shared.managedObjectContext)
    var burgerImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoadingView(SILoadingViewOptions(message: GlobalConstants.LoadingTextMessage, backgroundColor: UIColor.gray, fullScreen: true))
        self.barWithTint(UIColor.white, translucent: false, backButtonOptions: SINavigationBarBackButtonOptions(backButtonType: .none))
        self.setNavigationBarItem()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 0.0, right: 0.0)
        DataManager.shared.fetchMeals() {
            (completion: [Met]) in
            self.dataSource = completion
            self.hideLoadingView()
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let header = UIImageView(image: UIImage(named:GlobalConstants.HeaderMenuImageName))
        header.frame = CGRect(x: (self.navigationController?.navigationBar.frame.origin.x)!, y: (self.navigationController?.navigationBar.frame.origin.y)!, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)! + 20)
        self.navigationController?.view.addSubview(header)
        
        burgerImage = UIImageView(image: UIImage(named:GlobalConstants.BurgerMenuImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        burgerImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)!), size: burgerImage.frame.size)
        header.addSubview(burgerImage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.TitleCellReuseIdentifier, for: indexPath)
            cell.textLabel?.text = LocalizationConstants.MealWinesScreenTitleText
            let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0)
            let lineSeparator = UIView(frame: frame)
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        }
        
        let cell:MealsWinesTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.MealsWinesCellReuseIdentifier, for: indexPath) as! MealsWinesTableViewCell
        let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 2.0, width: GlobalConstants.ScreenWidth, height: 2.0)
        let lineSeparator = UIView(frame: frame)
        lineSeparator.backgroundColor = GlobalConstants.RedColor
        cell.addSubview(lineSeparator)
        cell.selectionStyle = .none

        let met = dataSource[indexPath.row - 1]
        let metName = met.met_name
        print(metName)
        cell.nameLabel.text = metName
        cell.nameLabel.textColor = GlobalConstants.RedColor
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return GlobalConstants.TitleCellHeight
        }
       return GlobalConstants.MealWinesCellHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:MealsWinesTableViewCell = tableView.cellForRow(at: indexPath)! as! MealsWinesTableViewCell
        selectedCell.contentView.backgroundColor = GlobalConstants.RedColor
        selectedCell.nameLabel.textColor = UIColor.white
        selectedCell.arrowImage.image = UIImage(named: GlobalConstants.TriangleRightWhiteImageName)
        met = dataSource[indexPath.row - 1]
        self.performSegue(withIdentifier: GlobalConstants.ShowWineAppelationForMealSegueName, sender: self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.ShowWineAppelationForMealSegueName {
            let destination = segue.destination as! WineAppelationForMealListTableViewController
            destination.met = met
        }
    }

}
