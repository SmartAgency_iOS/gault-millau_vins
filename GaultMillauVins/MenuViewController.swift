//
//  MenuViewController.swift
//  KNR
//
//  Created by Angel Antonov on 1/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var arrTitles: [String] = []
    var arrImages: [String] = []
    var tblMenu:UITableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrImages.append("magnifying_glass_menu_item")
        arrImages.append("favourites_menu_item")
        arrImages.append("meals_menu_item")
        arrImages.append("vintage_menu_item")
        arrImages.append("advices_menu_item")
        arrImages.append("legal_notes_menu_item")
        
        
        arrTitles.append(LocalizationConstants.SearchMenuItem)
        arrTitles.append(LocalizationConstants.MyFavouritesMenuItem)
        arrTitles.append(LocalizationConstants.MealsWinesMenuItem)
        arrTitles.append(LocalizationConstants.VintageMenuItem)
        arrTitles.append(LocalizationConstants.AdvicesMenuItem)
        arrTitles.append(LocalizationConstants.LegalNotesMenuItem)
        
        self.view.backgroundColor = #colorLiteral(red: 0.9093589187, green: 0.8857150674, blue: 0.8573153615, alpha: 1)
        
        //table
        tblMenu = UITableView(frame: CGRect(x: 0.0, y: 0.0, width: 0.7 * GlobalConstants.ScreenWidth, height: (CGFloat(arrTitles.count) * MenuCell.cellHeight) + 14.0), style: .plain)
        tblMenu.delegate = self
        tblMenu.dataSource = self
        tblMenu.isScrollEnabled = false
        tblMenu.backgroundColor = UIColor.clear
        
        //tblMenu.bottomShadow()
        
        self.view.addSubview(tblMenu)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        var count = 0
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            let favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
            count = favourites.count
        }
        
        arrTitles[1] = "\(LocalizationConstants.MyFavouritesMenuItem) (\(count))"
        
         NotificationCenter.default.addObserver(self, selector: #selector(changeCount(notification:)), name: NSNotification.Name(rawValue: "addedNewFavourite"), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "addedNewFavourite"), object: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuCell(style: .default, reuseIdentifier: NSStringFromClass(MenuCell.self), strImageName: arrImages[indexPath.row], strTitle: arrTitles[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toggleLeft()
        
        switch indexPath.row {
        case 0:
            GlobalConstants.AppDelegate.showMainStoryboard()
            break
        case 1:
            GlobalConstants.AppDelegate.showFavouritesStoryboard()
            break
        case 2:
            GlobalConstants.AppDelegate.showMealsWinesStoryboard()
            break
        case 3:
            GlobalConstants.AppDelegate.showVintageStoryboard()
            break
        case 4:
            GlobalConstants.AppDelegate.showAdvicesStoryboard()
            break
        case 5:
            GlobalConstants.AppDelegate.showLegalNotesStoryboard()
            break
        default:
            
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MenuCell.cellHeight
    }
    
    @objc func changeCount(notification:NSNotification) {
        var count = 0
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            let favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
            count = favourites.count
        }
        
        arrTitles[1] = "\(LocalizationConstants.MyFavouritesMenuItem) (\(count))"
        tblMenu.reloadData()
    }

}
