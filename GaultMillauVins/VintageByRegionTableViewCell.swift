//
//  VintageByRegionTableViewCell.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/29/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class VintageByRegionTableViewCell: UITableViewCell {
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var firstGlassImage: UIImageView!
    @IBOutlet weak var secondGlassImage: UIImageView!
    @IBOutlet weak var thirdGlassImage: UIImageView!
    @IBOutlet weak var fourthGlassImage: UIImageView!
    @IBOutlet weak var fifthGlassImage: UIImageView!
    @IBOutlet weak var ratingTextLabel: UILabel!
    var rate = [UIImageView]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rate = [firstGlassImage, secondGlassImage, thirdGlassImage, fourthGlassImage, fifthGlassImage]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
