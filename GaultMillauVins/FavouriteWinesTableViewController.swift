//
//  FavouriteWinesTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/23/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class FavouriteWinesTableViewController: UITableViewController, SINavigationBar {
    var favourites:[WineDataModel] = []
    var wine:Wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    var burgerImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeCount(notification:)), name: NSNotification.Name(rawValue: "addedNewFavourite"), object: nil)
        
        self.barWithTint(UIColor.white, translucent: false, backButtonOptions: SINavigationBarBackButtonOptions(backButtonType: .none))
        self.setNavigationBarItem()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
        } else {
            favourites = [WineDataModel]()
        }
        
        print(favourites)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let header = UIImageView(image: UIImage(named:GlobalConstants.HeaderMenuImageName))
        header.frame = CGRect(x: (self.navigationController?.navigationBar.frame.origin.x)!, y: (self.navigationController?.navigationBar.frame.origin.y)!, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)! + 20)
        self.navigationController?.view.addSubview(header)
        
        burgerImage = UIImageView(image: UIImage(named:GlobalConstants.BurgerMenuImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        burgerImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)!), size: burgerImage.frame.size)
        header.addSubview(burgerImage)
        
       
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByIncreasingPrice(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.IncreasingPriceNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByDecreasingPrice(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.DecreasingPriceNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByIncreasingYear(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.IncreasingVintageNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByDecreasingYear(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.DecreasingVintageNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeFavourite(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.MakeFavouriteNotificationName), object: nil)
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.IncreasingPriceNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.DecreasingPriceNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.IncreasingVintageNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.DecreasingVintageNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.MakeFavouriteNotificationName), object: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favourites.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell:BaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.BaseCellIdentifier, for: indexPath) as! BaseTableViewCell
            let wineCount = favourites.count
            let string = LocalizationConstants.Favourites
            cell.winesCountLabel.text = "\(wineCount) \(string)"
            let lineSeparator = UIView(frame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth - 60, height: 1.0))
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        }
        
        let cell:WineTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.WineCellIdentifier, for: indexPath) as! WineTableViewCell
        let wine = favourites[indexPath.row - 1]
        
        let image = UIImage(named: GlobalConstants.UnfavouriteButtonIconImageName)
        cell.favouriteButton.setImage(image, for: .normal)
        
        cell.wineAppelationLabel.text = wine.wineAppeleation
        cell.wineDomainLabel.text = wine.wineDomainName
        let value = Double(wine.wineNote)
        cell.wineNoteLabel.text = String.localizedStringWithFormat("%.1f", value!)
        cell.wineTextLabel.text = wine.wineText
        if wine.winePrice != 0 {
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = NumberFormatter.Style.currency
            currencyFormatter.locale = NSLocale.current
            let priceString = currencyFormatter.string(from: wine.winePrice)
            cell.winePriceLabel.text = priceString
        } else {
            cell.winePriceLabel.text = GlobalConstants.PriceNotAvailableText
        }
        cell.wineRegionLabel.text = wine.wineRegion
        var text = String()
        if wine.wineVintage != 1111 {
            text = String(format: "\(wine.wineBrand) \(wine.wineClassification) \(wine.wineCuvee) \(wine.wineVintage), \(wine.wineColor)")
        } else {
            text = String(format: "\(wine.wineBrand) \(wine.wineClassification) \(wine.wineCuvee), \(wine.wineColor)")
        }
        cell.wineDescription.text = text
        cell.favouriteButton.tag = indexPath.row - 1
        let lineSeparator = UIView(frame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth - 60, height: 1.0))
        lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
        cell.addSubview(lineSeparator)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row != 0) {
            return GlobalConstants.WineCellHeight
        }
        
        return GlobalConstants.BaseCellHeight
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if(indexPath.row == 0) {
            return nil
        }
        
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(favourites[indexPath.row - 1].wineAppeleation)
        DataManager.shared.fetchChosenFavouriteWine(wine: favourites[indexPath.row - 1]) {
            (completion: Wine) in
            self.wine = completion
            self.performSegue(withIdentifier: GlobalConstants.ShowFavouriteWinesSegueName, sender: self)
        }
    }
    
    @objc func sortDataByIncreasingPrice(notification: NSNotification) {
        favourites.sort() {(($0.winePrice as Decimal) - ($1.winePrice as Decimal)) < 0}
        tableView.reloadData()
    }
    
    @objc func sortDataByDecreasingPrice(notification: NSNotification) {
        favourites.sort() {(($0.winePrice as Decimal) - ($1.winePrice as Decimal)) > 0}
        tableView.reloadData()
    }
    
    @objc func sortDataByIncreasingYear(notification: NSNotification) {
        favourites.sort() {$0.wineVintage < $1.wineVintage}
        tableView.reloadData()
    }
    
    @objc func sortDataByDecreasingYear(notification: NSNotification) {
        favourites.sort() {$0.wineVintage > $1.wineVintage}
        tableView.reloadData()
    }
    
    @objc func makeFavourite(notification:NSNotification) {
        let buttonRow = notification.object as! Int
        let wine = favourites[buttonRow]
        if let index = favourites.index(of: wine) {
            favourites.remove(at: index)
        }
    
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: favourites as NSArray)
        UserDefaults.standard.set(encodedData, forKey:GlobalConstants.FavouriteWinesKey)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.AddedNewFavouriteWineNotificationName), object: nil)
        tableView.reloadData()
    }
    
    @objc func changeCount(notification:NSNotification) {
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
        }
        
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == GlobalConstants.ShowFavouriteWinesSegueName) {
            if let destination = segue.destination as? WineDetailViewController {
                destination.wine = wine
            }
        }
    }
}
