//
//  DetailedMapViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/17/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class DetailedMapViewController: UIViewController, MKMapViewDelegate {
    var mapView:MKMapView?
    @IBOutlet var domainAnnotationView: DomainAnnotationView!
    let point = DomainPointAnnotation()
    var pinAnnotationView: MKPinAnnotationView!

    var location = String()
    var wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(callDomain(notification:)), name: NSNotification.Name(rawValue: "callDomain"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        mapView = MKMapView(frame: self.view.frame)
        mapView?.mapType = .standard
        self.view.addSubview(mapView!)
        mapView?.delegate = self
        if (wine.wine_domain!.domain_address?.address_latitude.isEqual(to: 0.000000))! {
            let coordinates = CLLocationCoordinate2DMake(46.604167, 2.263184)
            self.mapView?.setRegion(MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 3, longitudeDelta: 3)), animated: true)
            self.mapView?.centerCoordinate = coordinates
        } else {
            let coordinates = CLLocationCoordinate2DMake((wine.wine_domain!.domain_address?.address_latitude)!, (wine.wine_domain!.domain_address?.address_longitute)!)
            self.mapView?.setRegion(MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 0.007, longitudeDelta: 0.007)), animated: true)
            self.mapView?.centerCoordinate = coordinates
            self.point.image = GlobalConstants.WineDetailsPinImageName
            self.point.coordinate = coordinates
            self.pinAnnotationView = MKPinAnnotationView(annotation: self.point, reuseIdentifier: GlobalConstants.AnnotationViewReuseIdentifier)
            self.mapView?.addAnnotation(self.pinAnnotationView.annotation!)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        mapView?.mapType = .hybrid
        mapView?.removeAnnotations((self.mapView?.annotations)!)
        mapView?.removeFromSuperview()
        mapView?.delegate = nil
        mapView = nil
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        mapView?.removeFromSuperview()
        mapView?.delegate = nil
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = GlobalConstants.AnnotationViewReuseIdentifier
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = false
        } else {
            annotationView?.annotation = annotation
        }
        
        annotationView?.detailCalloutAccessoryView = domainAnnotationView
        
        let customPointAnnotation = annotation as! DomainPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.image)
        
        return annotationView
    }
    
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        domainAnnotationView.domainNameLabel.text = wine.wine_domain!.domain_name
        domainAnnotationView.domainAddressLabel.text = wine.wine_domain!.domain_address!.address_city!
        domainAnnotationView.domainPostalCodeAndCityLabel.text = "\(wine.wine_domain!.domain_post_code!), \(wine.wine_domain!.domain_address!.address_name!)"
        domainAnnotationView.domainPhoneLabel.setTitle("Tel: \(wine.wine_domain!.domain_phone!)", for: .normal)
        
        let calloutView = domainAnnotationView
        calloutView?.center = CGPoint(x: view.bounds.size.width / 2, y: (calloutView?.bounds.size.height)! * -0.52)
        view.addSubview(calloutView!)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
       
    @objc func callDomain(notification: NSNotification) {
        let phone = wine.wine_domain!.domain_phone!.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL, completionHandler:nil)
            print(phone)
        }
    }

    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self) {
            for subview in view.subviews {
                subview.removeFromSuperview()
            }
        }
    }
    
}
