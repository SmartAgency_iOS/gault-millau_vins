//
//  DataParser.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/7/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import CoreData

final class DataParser: NSObject {
    static let shared = DataParser()
    let dbContext = DataBaseContext.shared.managedObjectContext
    
    private override init() {
    }
    
    
    private func wineFileSplit(wineData:[[String]]) {
        print("WINES START")
        for object in wineData {
            let wine = Wine(context: self.dbContext)
            wine.wine_appelation = object[5]
            wine.wine_cuvee = object[4]
            wine.wine_brand = object[3]
            wine.wine_classification = object[6]
            wine.wine_note = object[9]
            wine.wine_text = object[11]
            if object[10] != "" {
                wine.wine_price = Decimal.init(string: object[10]) as NSDecimalNumber?
            } else {
                wine.wine_price = Decimal.init(0) as NSDecimalNumber?
            }
            wine.wine_vintage = (Int32.init(object[8]) as Int32?)!
            self.dbContext.insert(wine)
            let wineID = wine.objectID
            let added = self.dbContext.object(with: wineID) as! Wine
            
            // relation with domain
            let domainRequest:NSFetchRequest<Domain> = Domain.fetchRequest()
            var predicate = NSPredicate(format:DbConstants.DomainNameFormat, object[19])
            domainRequest.predicate = predicate
            self.dbContext.perform {
                do {
                    let result = try domainRequest.execute()
                    let domainResult = try domainRequest.execute().first
                    domainResult?.addToDomain_wine(added)
                    added.wine_domain = domainResult
                    try self.dbContext.save()
                    
                    // to be cleaned
                    print("--------------------------------------------")
                    print(domainResult ?? "failed")
                    print(added)
                    print(result.count)
                    print("..................................")
                    
                } catch {
                    // add code later
                }
            }
            
            // relationship with region
            let regionRequest:NSFetchRequest<Region> = Region.fetchRequest()
            predicate = NSPredicate(format:DbConstants.RegionNameFormat, object[12])
            regionRequest.predicate = predicate
            self.dbContext.perform {
                do {
                    let result = try regionRequest.execute()
                    let regionResult = try regionRequest.execute().first
                    regionResult?.addToRegion_wine(added)
                    added.wine_region = regionResult
                    try self.dbContext.save()
                    
                    // to be cleaned
                    print("--------------------------------------------")
                    print(regionResult ?? "failed")
                    print(added)
                    print(result.count)
                    print("..................................")
                    
                } catch {
                    // add code later
                }
            }
            
            // relationship with color
            let colorRequest:NSFetchRequest<Color> = Color.fetchRequest()
            predicate = NSPredicate(format:DbConstants.ColorNameFormat, object[7])
            colorRequest.predicate = predicate
            self.dbContext.perform {
                do {
                    let result = try colorRequest.execute()
                    let colorResult = try colorRequest.execute().first
                    colorResult?.addToColor_wine(added)
                    added.wine_color = colorResult
                    try self.dbContext.save()
                    
                    // to be cleaned
                    print("--------------------------------------------")
                    print(colorResult ?? "failed")
                    print(added)
                    print(result.count)
                    print("..................................")
                    
                    
                } catch {
                    // add code later
                }
            }
        }
        
        if self.dbContext.hasChanges {
            do {
                try self.dbContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
                
        print("WINE END")
    }
    
    private func domainSplit(wineData:[[String]]) {
        var data = Set<String>()
        var count = data.count
        for object in wineData {
            let domainName = object[19]
            data.update(with: domainName)
            if(count != data.count) {
                let domain = Domain(context: self.dbContext)
                domain.domain_name = domainName
                domain.domain_email = object[17]
                domain.domain_phone = object[16]
                domain.domain_post_code = object[14]
                domain.domain_website = object[18]

                let address = Address(context: dbContext)
                address.address_name = object[15]
                address.address_city = object[13]
                if object.count == 22 && object[20] != "" && object[20] != nil{
                    address.address_longitute = Double(object[21])!
                    address.address_latitude = Double(object[20])!
                } else {
                    address.address_longitute = 0.0000000
                    address.address_latitude = 0.0000000
                }
                address.address_domain = domain
                domain.domain_address = address
                self.dbContext.insert(domain)
                count += 1
                
                // to be cleaned
                print(domain)
                print("DOMAIN COUNT = %@", count)
                print("...............................")
            }
        }
        
        if self.dbContext.hasChanges {
            do {
                try self.dbContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func regionSplit(wineData:[[String]]) {
        var data = Set<String>()
        var count = data.count
        for object in wineData {
            let regionName = object[12]
            data.update(with: regionName)
            if(count != data.count) {
                let region = Region(context: self.dbContext)
                region.region_name = regionName
                self.dbContext.insert(region)
                count += 1
                
                // to be cleaned
                print(region)
                print("REGION COUNT = %@", count)
                print("...............................")
            }
        }
        
        if self.dbContext.hasChanges {
            do {
                try self.dbContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func colorSplit(wineData:[[String]]) {
        var data = Set<String>()
        var count = data.count
        for object in wineData {
            let colorName = object[7]
            data.update(with: colorName)
            if(count != data.count) {
                let color = Color(context: self.dbContext)
                color.color_name = colorName
                self.dbContext.insert(color)
                count += 1
                
                // to be cleaned
                print(color)
                print("COLOR COUNT = %@", count)
                print("...............................")
            }
            
    }
        
        if self.dbContext.hasChanges {
            do {
                try self.dbContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func metFileSplit(metData:[[String]], metWineData:[[String]]) {
        for object in metData {
            let met = Met(context: self.dbContext)
            met.met_name = object[3]
            met.met_description = object[4]
            self.dbContext.insert(met)
        }
        
        if self.dbContext.hasChanges {
            do {
                try self.dbContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        
        for object in metData {
                let metName = object[3]
                let filtered = metWineData.filter{$0[4] == metName}
        
            for filteredObject in filtered {
                let wineRequest:NSFetchRequest<Wine> = Wine.fetchRequest()
                let winePredicate = NSPredicate(format: DbConstants.WineAppelationFormat, filteredObject[3])
                wineRequest.predicate = winePredicate
                let metRequest:NSFetchRequest<Met> = Met.fetchRequest()
                let metPredicate = NSPredicate(format: DbConstants.MetNameFormat, filteredObject[4])
                metRequest.predicate = metPredicate
                
                self.dbContext.perform {
                    do {
                        let result = try wineRequest.execute()
                        let metResult = try metRequest.execute().first
                        for r in result {
                            r.addToWine_met(metResult!)
                        }
                        
                        metResult?.addToMet_wine(NSSet(array: result))
                        
                        // to be cleaned
                        print("--------------------------------------------")
                        print(metResult ?? "failed")
                        print(result)
                        print(result.count)
                        print("..................................")

                    } catch {
                        // add code later
                    }
                    
                    if self.dbContext.hasChanges {
                        do {
                            try self.dbContext.save()
                        } catch {
                            let nserror = error as NSError
                            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                        }
                    }
                }
            
                if self.dbContext.hasChanges {
                    do {
                        try self.dbContext.save()
                    } catch {
                        let nserror = error as NSError
                        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                    }
                }
            }
        }
    }
    
    private func saveVintage(vintageData: [[String]]) {
        var arrOfVintageDataModel = [NSData]()
        var arrOfYears = [Int]()

        for object in vintageData {
            let index = vintageData.index(where: {$0 == object})
            if(index == 0) {
                for part in  object {
                     let ind = object.index(where:{$0 == part})
                    if(ind! < 1) {
                        continue
                    }
                    
                    let i = part.index(part.startIndex, offsetBy: 2)
                    let yearAsString = part.substring(from: i)
                    let year = Int.init(yearAsString)
                    arrOfYears.append(year!)
                }
                
                continue
            }
            
            let name = object[0]
            var ratingForYear = [Int:Int]()
            var i = 0
            for part in object {
                if(part == object[0]) {
                    continue
                }
                let key = arrOfYears[i]
                ratingForYear[key] = 0
                ratingForYear.updateValue(Int.init(part)!, forKey: arrOfYears[i])
                i += 1
            }
            
            var vintageAsDictionary = Dictionary<String, Any>()
            vintageAsDictionary[DbConstants.VintageNameKey] = name
            vintageAsDictionary[DbConstants.RatingForYearKey] = ratingForYear
            
            let vintage = VintageDataModel(dictionary: vintageAsDictionary)
            print(vintage)
            let dict = vintage.encode()
            arrOfVintageDataModel.append(dict)
        }
        UserDefaults.standard.set(arrOfVintageDataModel, forKey: DbConstants.VintageEntityName)
        UserDefaults.standard.synchronize()
    }
    
    private func saveAdvice(adviceData: [[String]]) {
        var arrOfAdvices = [NSData]()
        for object in adviceData {
            let adviceName = object[3]
            let adviceDescription = object[4]
            let dictionary = Dictionary(dictionaryLiteral: (DbConstants.AdviceNameKey, adviceName),
                                        (DbConstants.AdviceDescriptionKey, adviceDescription))
            let advice = Advice(dictionary: dictionary)
            print(advice)
            let data = advice.encode()
            arrOfAdvices.append(data)
        }
        
        UserDefaults.standard.set(arrOfAdvices, forKey: DbConstants.AdviceEntityName)
        UserDefaults.standard.synchronize()
    }

        
     private func parseCSVFile(url:URL) -> [[String]] {
        let delimiter = ","
        var arrayOfValues = [[String]]()
        
        if let data = NSData(contentsOf:url) {
            if let content = NSString(data: data as Data, encoding: DbConstants.encoding) {
                let lines:[String] = content.components(separatedBy: NSCharacterSet.newlines) as [String]
                
                for line in lines {
                    var values = [String]()
                    var value:String = ""
                    
                    if (line.range(of: "\"") != nil) {
                        var textToScan = line
                        var textScanner = Scanner(string: line)
                        let textScannerValue = AutoreleasingUnsafeMutablePointer<NSString?>(&value)
                        
                        while (textScanner.string != "") {
                            if (textScanner.string as NSString).substring(to: 1) == "\"" {
                                textScanner.scanLocation += 1
                                textScanner.scanUpTo("\"", into: textScannerValue)
                                textScanner.scanLocation += 1
                            } else {
                                textScanner.scanUpTo(delimiter, into: textScannerValue)
                            }
                            
                            // Store the value into the values array
                            values.append(textScannerValue.pointee as!(String))
                            
                            // Retrieve the unscanned remainder of the string
                            if textScanner.scanLocation < textScanner.string.characters.count {
                                textToScan = (textScanner.string as NSString).substring(from: textScanner.scanLocation + 1)
                            } else {
                                textToScan = ""
                            }
                            textScanner = Scanner(string: textToScan)
                        }
                    } else {
                        values = line.components(separatedBy: delimiter)
                    }
                    
                    if(values.count > 1) {
                        arrayOfValues.append(values)
                    }
                }
            }
        }
        
        print(arrayOfValues.count)
        return arrayOfValues
    }
        
    func populateDb() {
//        // populate domain, wine, region and color tables
//        let wineData = parseCSVFile(url: URL(fileReferenceLiteralResourceName: "ZWINE_LAT_LONG.csv"))
//        domainSplit(wineData: wineData)
//        regionSplit(wineData: wineData)
//        colorSplit(wineData: wineData)
//        wineFileSplit(wineData: wineData)
//        
//        // populate met, met_wine/wine_met relation
//        let metData = parseCSVFile(url: URL(fileReferenceLiteralResourceName: DbConstants.MetFileName))
//        let metWineData = parseCSVFile(url: URL(fileReferenceLiteralResourceName: DbConstants.MealWineFileName))
//        metFileSplit(metData: metData, metWineData:metWineData)

        // parse vintage and save them in UserDefaults
        let vintageData = parseCSVFile(url: URL(fileReferenceLiteralResourceName: DbConstants.VintageFileName))
        saveVintage(vintageData: vintageData)
        
        // parse advices and save them in UserDefaults
        let adviceData = parseCSVFile(url: URL(fileReferenceLiteralResourceName: DbConstants.AdviceFileName))
        saveAdvice(adviceData: adviceData)
        
        // to be cleaned
        let wines:NSFetchRequest<Wine> = Wine.fetchRequest()
        let format = "Gewurztraminer Vendanges Tardives"
        let predicate = NSPredicate(format:"wine_appelation == %@", format)
        wines.predicate = predicate
        self.dbContext.perform {
            do {
                let result = try wines.execute()
                
                for r in result {
                    print("--------------------------------------------")
                    print(r.wine_met!)
                    print(result.count)
                    print("..................................")
                }
                
            } catch {
                // add code later
            }
        }

    }
}


