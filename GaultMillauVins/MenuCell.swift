//
//  MenuCell.swift
//  KNR
//
//  Created by Angel Antonov on 1/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    var imgItem: UIImageView!
    var lblTitle: UILabel!
    
    init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, strImageName: String!, strTitle: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
       let img = UIImage(named: strImageName)!
        self.imageView?.image = img
        self.textLabel?.text = strTitle
        self.textLabel?.textColor = #colorLiteral(red: 0.4972863793, green: 0.4497597218, blue: 0.3888614774, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.9093589187, green: 0.8857150674, blue: 0.8573153615, alpha: 1)
        let line = UIView(frame: CGRect(x: 0.0, y: MenuCell.cellHeight - 1.0, width: GlobalConstants.ScreenWidth, height: 1.0))
        line.backgroundColor = #colorLiteral(red: 0.8346485496, green: 0.8033455014, blue: 0.7749363184, alpha: 1)
        self.addSubview(line)
        
        self.selectionStyle = .none
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class var cellHeight: CGFloat {
        return 60.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
