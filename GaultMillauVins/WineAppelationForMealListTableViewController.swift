//
//  WineAppelationForMealListTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/24/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class WineAppelationForMealListTableViewController: UITableViewController, SILoadingViewRenderer {
    var met:Met = Met(context: DataBaseContext.shared.managedObjectContext)
    var dataSource = [String]()
    var wineAppelation = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoadingView(SILoadingViewOptions(message: GlobalConstants.LoadingTextMessage, backgroundColor: UIColor.gray, fullScreen: true))
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let backButtonImage = UIImageView(image: UIImage(named:GlobalConstants.BackButtonImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        backButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 10, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size: backButtonImage.frame.size)
        self.navigationController?.view.addSubview(backButtonImage)
        self.tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        DataManager.shared.fetchWineAppelationsForMet(met: met) {
            (result:[String]) in
            self.dataSource = result
            self.hideLoadingView()
            self.tableView.reloadData()
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count + 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.TitleCellReuseIdentifier, for: indexPath)
            cell.textLabel?.text = LocalizationConstants.MealWinesScreenTitleText
            let frame = CGRect(x: cell.frame.origin.x, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0)
            let lineSeparator = UIView(frame: frame)
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.MealCellReuseIdentifier, for: indexPath)
            cell.textLabel!.text = met.met_name.uppercased()
            cell.detailTextLabel!.text = met.met_description
            return cell
        }
        
        let cell:MealsWinesTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.WineAppellationCellIdentifier, for: indexPath) as! MealsWinesTableViewCell
        cell.nameLabel.text = dataSource[indexPath.row - 2]
        let frame = CGRect(x: cell.frame.origin.x + 20, y: cell.frame.size.height - 1.0, width: GlobalConstants.ScreenWidth - 40, height: 1.0)
        let lineSeparator = UIView(frame: frame)
        lineSeparator.backgroundColor = GlobalConstants.RedColor
        cell.addSubview(lineSeparator)
        cell.selectionStyle = .none
        cell.nameLabel.textColor = UIColor.black
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 1 {
            let selectedCell:MealsWinesTableViewCell = tableView.cellForRow(at: indexPath)! as! MealsWinesTableViewCell
            selectedCell.contentView.backgroundColor = GlobalConstants.RedColor
            selectedCell.nameLabel.textColor = UIColor.white
            wineAppelation = dataSource[indexPath.row - 2]
            self.performSegue(withIdentifier: GlobalConstants.ShowWinesForAppelationSegueName, sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return GlobalConstants.TitleCellHeight
        } else {
            return GlobalConstants.MealWinesCellHeight
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.ShowWinesForAppelationSegueName {
            let destination = segue.destination as! WineForAppelationListTableViewController
            destination.wineAppelation = wineAppelation
            destination.metName = met.met_name
        }
    }


}
