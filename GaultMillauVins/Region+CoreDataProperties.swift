//
//  Region+CoreDataProperties.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/4/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import Foundation
import CoreData


extension Region {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Region> {
        return NSFetchRequest<Region>(entityName: DbConstants.RegionEntityName);
    }

    @NSManaged public var region_name: String
    @NSManaged public var region_wine: NSSet

}


// MARK: Generated accessors for region_wine
extension Region {

    @objc(addRegion_wineObject:)
    @NSManaged public func addToRegion_wine(_ value: Wine)

    @objc(removeRegion_wineObject:)
    @NSManaged public func removeFromRegion_wine(_ value: Wine)

    @objc(addRegion_wine:)
    @NSManaged public func addToRegion_wine(_ values: NSSet)

    @objc(removeRegion_wine:)
    @NSManaged public func removeFromRegion_wine(_ values: NSSet)

}
