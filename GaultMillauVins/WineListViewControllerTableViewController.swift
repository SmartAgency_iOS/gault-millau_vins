//
//  WineListViewControllerTableViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/16/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import CoreData
import Branch

class WineListViewControllerTableViewController: UITableViewController, SILoadingViewRenderer {
    var dataSource:[Wine] = []
    var wine:Wine = Wine(context: DataBaseContext.shared.managedObjectContext)
    var favourites:[WineDataModel] = []
    var keyword:String?
    var regionCriteria:String?
    var colorCriteria:String?
    var vintageCriteria:String?
    var shareButtonImage:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
        self.tableView.registerCellClass(WineTableViewCell.self)
        self.showLoadingView(SILoadingViewOptions(message: GlobalConstants.LoadingTextMessage, backgroundColor: UIColor.gray, fullScreen: true))
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let backButtonImage = UIImageView(image: UIImage(named:GlobalConstants.BackButtonImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        backButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size: backButtonImage.frame.size)
        self.navigationController?.view.addSubview(backButtonImage)
        
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
        } else {
            favourites = [WineDataModel]()
        }

        let shareButton = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(displayShareSheet(sender:)))
        self.navigationItem.rightBarButtonItem = shareButton
    }

    @objc func displayShareSheet(sender:UIBarButtonItem) {
        var regionCriteriaEncoded, colorCriteriaEncoded, vintageCriteriaEncoded, freeCriteriaEncoded : String?
        var url = ""
        if regionCriteria != nil && regionCriteria != "" {
            regionCriteriaEncoded = regionCriteria?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
            url.append("region=\(regionCriteriaEncoded!)=")
        }
        if colorCriteria != nil && colorCriteria != "" {
            colorCriteriaEncoded = colorCriteria?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
            url.append("color=\(colorCriteriaEncoded!)=")
        }
        if vintageCriteria != nil && vintageCriteria != "" {
            vintageCriteriaEncoded = vintageCriteria?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
            url.append("vintage=\(vintageCriteriaEncoded!)=")
        }

        if keyword != nil && keyword != "" {
            freeCriteriaEncoded = keyword?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
            url.append("key=\(freeCriteriaEncoded!)=")
        }

        let deepLinkString = "wine://search/\(url)"

        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "search/\(url)")
        branchUniversalObject.title = LocalizationConstants.FacebookShareTitle
        branchUniversalObject.contentDescription = LocalizationConstants.ShareWineListText
        branchUniversalObject.addMetadataKey("id", value: "\(url)")

        print(url)
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        linkProperties.addControlParam("$ios_url", withValue: deepLinkString)
        linkProperties.addControlParam("$email_subject", withValue: LocalizationConstants.MailSubject)
        linkProperties.channel = "facebook"
        branchUniversalObject.showShareSheet(with: linkProperties,
                                             andShareText: LocalizationConstants.ShareWineListText,
                                             from: self) {
                                                (activityType, completed) in
                                                NSLog("done showing share sheet!")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        shareButtonImage = UIImageView(image: UIImage(named: "share")?.withRenderingMode(.alwaysOriginal))
        shareButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.size.width)! - 50, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size:  shareButtonImage.frame.size)
        self.navigationController?.view.addSubview(shareButtonImage)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByIncreasingPrice(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.IncreasingPriceNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByDecreasingPrice(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.DecreasingPriceNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByIncreasingYear(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.IncreasingVintageNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sortDataByDecreasingYear(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.DecreasingVintageNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeFavourite(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.MakeFavouriteNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeCount(notification:)), name: NSNotification.Name(rawValue: "addedNewFavourite"), object: nil)
        self.hideLoadingView()
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        shareButtonImage.removeFromSuperview()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.IncreasingPriceNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.DecreasingPriceNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.IncreasingVintageNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.DecreasingVintageNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.MakeFavouriteNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "addedNewFavourite"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return dataSource.count + 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell:BaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.BaseCellIdentifier, for: indexPath) as! BaseTableViewCell
            let wineCount = dataSource.count
            cell.selectionStyle = .none
            let string = LocalizationConstants.Wines.lowercased()
            cell.winesCountLabel.text = "\(wineCount) \(string)"
            let lineSeparator = UIView(frame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth - 60, height: 1.0))
            lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
            cell.addSubview(lineSeparator)
            return cell
        }

        let cell:WineTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalConstants.WineCellIdentifier, for: indexPath) as! WineTableViewCell
        let wine = dataSource[indexPath.row - 1]
        
        let wineData = WineDataModel(wineAppeleation: wine.wine_appelation ?? "", wineCuvee: wine.wine_cuvee ?? "", wineBrand: wine.wine_brand ?? "", wineClassification: wine.wine_classification ?? "", wineNote:wine.wine_note, winePrice: (wine.wine_price ?? 0) as NSDecimalNumber, wineText: wine.wine_text, wineVintage: wine.wine_vintage, wineDomainName: wine.wine_domain!.domain_name!, wineRegion: wine.wine_region!.region_name , wineColor: wine.wine_color!.color_name)
        
        let image = UIImage(named: GlobalConstants.FavouriteButtonIconImageName)
        cell.favouriteButton.setImage(image, for: .normal)
        
        for object in favourites {
            if checkIfEqual(object: object, wineData: wineData) {
                cell.favouriteButton.setImage(UIImage(named: GlobalConstants.UnfavouriteButtonIconImageName), for: .normal)
            }
        }
        
        cell.wineAppelationLabel.text = wine.wine_appelation
        cell.wineDomainLabel.text = wine.wine_domain!.domain_name
        let formatter = NumberFormatter()
        formatter.locale = NSLocale.current
        let value = Double(wine.wine_note)
        cell.wineNoteLabel.text = String.localizedStringWithFormat("%.1f", value!)
        cell.wineTextLabel.text = wine.wine_text
        if wine.wine_price != 0 {
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = NumberFormatter.Style.currency
            currencyFormatter.locale = NSLocale.current
            let priceString = currencyFormatter.string(from: wine.wine_price!)
            cell.winePriceLabel.text = priceString
        } else {
            cell.winePriceLabel.text = GlobalConstants.PriceNotAvailableText
        }
        cell.wineRegionLabel.text = wine.wine_region?.region_name
        var text = String()
        if wine.wine_vintage != 1111 {
            text = String(format: "\(wine.wine_brand!) \(wine.wine_classification!) \(wine.wine_cuvee!) \(wine.wine_vintage), \(wine.wine_color!.color_name)")
        } else {
            text = String(format: "\(wine.wine_brand!) \(wine.wine_classification!) \(wine.wine_cuvee!), \(wine.wine_color!.color_name)")
        }
        cell.wineDescription.text = text
        cell.favouriteButton.tag = indexPath.row - 1

        if self.keyword != "" && self.keyword != nil {
            var range = ((text.lowercased()) as NSString).range(of: keyword!)
            if range.location != NSNotFound {
                let attributes: [NSAttributedString.Key:Any] = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
                let attributedText = NSMutableAttributedString(string: text)
                attributedText.addAttributes(attributes, range: range)
                cell.wineDescription.attributedText = attributedText
            }

            range = (wine.wine_text as NSString).range(of:keyword!)
            if range.location != NSNotFound {
                let attributes: [NSAttributedString.Key:Any] = [NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue]
                let attributedText = NSMutableAttributedString(string: wine.wine_text)
                attributedText.addAttributes(attributes, range: range)
                cell.wineTextLabel.attributedText = attributedText
            }

            range = ((wine.wine_domain!.domain_name!.lowercased()) as NSString).range(of:keyword!)
            if range.location != NSNotFound {
                let attributes: [NSAttributedString.Key:Any] = [NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue]
                let attributedText = NSMutableAttributedString(string: wine.wine_domain!.domain_name!)
                attributedText.addAttributes(attributes, range: range)
                cell.wineDomainLabel.attributedText = attributedText
            }

        }
        
        let lineSeparator = UIView(frame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: GlobalConstants.ScreenWidth - 60, height: 1.0))
        lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
        cell.addSubview(lineSeparator)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row != 0) {
            return GlobalConstants.WineCellHeight
        }
        
        return GlobalConstants.BaseCellHeight
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if(indexPath.row == 0) {
            return nil
        }
        
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        wine = dataSource[indexPath.row - 1]
        self.performSegue(withIdentifier: GlobalConstants.WineDetailsSegueIdentifier, sender: self)
    }
    
    @objc func sortDataByIncreasingPrice(notification: NSNotification) {
        dataSource.sort() {(($0.wine_price! as Decimal) - ($1.wine_price! as Decimal)) < 0}
        tableView.reloadData()
    }
    
    @objc func sortDataByDecreasingPrice(notification: NSNotification) {
        dataSource.sort() {(($0.wine_price! as Decimal) - ($1.wine_price! as Decimal)) > 0}
        tableView.reloadData()
    }
    
    @objc func sortDataByIncreasingYear(notification: NSNotification) {
        dataSource.sort() {$0.wine_vintage < $1.wine_vintage}
        tableView.reloadData()
    }
    
    @objc func sortDataByDecreasingYear(notification: NSNotification) {
        dataSource.sort() {$0.wine_vintage > $1.wine_vintage}
        tableView.reloadData()
    }
    
    @objc func makeFavourite(notification:NSNotification) {
        let buttonRow = notification.object as! Int
        let wine = dataSource[buttonRow]
        let count = favourites.count

        let wineData = WineDataModel(wineAppeleation: wine.wine_appelation ?? "", wineCuvee: wine.wine_cuvee ?? "", wineBrand: wine.wine_brand ?? "", wineClassification: wine.wine_classification ?? "", wineNote:wine.wine_note, winePrice: (wine.wine_price ?? 0) as NSDecimalNumber, wineText: wine.wine_text, wineVintage: wine.wine_vintage, wineDomainName: wine.wine_domain!.domain_name!, wineRegion: wine.wine_region!.region_name, wineColor: wine.wine_color!.color_name)
        
        for object in favourites {
            if checkIfEqual(object: object, wineData: wineData) {
                if let index = favourites.index(of: object) {
                    favourites.remove(at: index)
                    print(favourites.count)
                }
            }
        }
        
        if(count == favourites.count) {
            favourites.append(wineData)
            print(favourites.count)
        }

        let encodedData = NSKeyedArchiver.archivedData(withRootObject: favourites as NSArray)
        UserDefaults.standard.set(encodedData, forKey:GlobalConstants.FavouriteWinesKey)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.AddedNewFavouriteWineNotificationName), object: nil)

    }
    
    func checkIfEqual(object:WineDataModel, wineData:WineDataModel)->(Bool) {
        return object.wineAppeleation == wineData.wineAppeleation && object.wineCuvee == wineData.wineCuvee && object.wineBrand == wineData.wineBrand && object.wineClassification == wineData.wineClassification && object.wineNote == wineData.wineNote && object.winePrice == wineData.winePrice && object.wineText == wineData.wineText
    }
    
    @objc func changeCount(notification:NSNotification) {
        if let data = UserDefaults.standard.data(forKey:GlobalConstants.FavouriteWinesKey) {
            favourites = (NSKeyedUnarchiver.unarchiveObject(with: data) as! [WineDataModel])
        }
        
        tableView.reloadData()
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == GlobalConstants.WineDetailsSegueIdentifier) {
            if let destination = segue.destination as? WineDetailViewController {
                destination.wine = wine
            }
        }
    }
}
