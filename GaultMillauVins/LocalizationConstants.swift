//
//  LocalizationConstants.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 12/2/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

struct LocalizationConstants {
    static let RegionOfOrigin =  "regionOfOrigin".localized
    static let WineColor = "wineColor".localized
    static let Vintage = "vintage".localized
    static let Search = "search".localized
    static let Wines = "wines".localized
    static let WinePlaceholed = "winePlaceHolder".localized
    static let ChooseCriteriaLabelText = "chooseCriteriaLabelText".localized
    static let AdditionalCriteriaLabelText = "additionalCriteriaLabelText".localized
    static let SortResult = "sortResults".localized
    static let SortByIncreasingPrice = "sortByIncreasingPrice".localized
    static let SortBydecreasingPrice = "sortBydecreasingPrice".localized
    static let SortByIncreasingYear = "sortByIncreasingYear".localized
    static let SortBydecreasingYear = "sortBydecreasingYear".localized
    static let RegionLabelText = "regionLabelText".localized
    static let AppellationLabelText = "appellationLabelText".localized
    static let WineDescriptionTitleLabelText = "wineDescriptionTitleLabelText".localized
    static let MealsWineTitleLabelText = "mealsWineTitleLabelText".localized
    static let SeeOnMapButtonText = "seeOnMapButtonText".localized
    static let CallDomainButtonText = "callDomainButtonText".localized
    static let Favourites = "favourites".localized
    static let MealWinesScreenTitleText = "mealWinesScreenTitleText".localized
    static let NameOfWineText = "nameOfWineText".localized
    static let NoteText = "noteText".localized
    static let MillesimeTitleLabelText = "millesimeTitleLabelText".localized
    static let RegionListText = "regionListText".localized
    static let VintageListText = "vintageListText".localized
    static let AdvicesTitleText = "advicesTitleText".localized
    static let ChooseRegionLabelText = "chooseRegionLabelText".localized
    static let ChooseVintageLabelText = "chooseVintageLabelText".localized
    static let PoorYear = "poorYear".localized
    static let AverageYear = "averageYear".localized
    static let GoodYear = "goodYear".localized
    static let VeryGoodYear = "veryGoodYear".localized
    static let Exceptional = "exceptional".localized
    static let SearchMenuItem = "searchMenuItem".localized
    static let MyFavouritesMenuItem = "myFavouritesMenuItem".localized
    static let MealsWinesMenuItem = "mealsWinesMenuItem".localized
    static let VintageMenuItem = "vintageMenuItem".localized
    static let AdvicesMenuItem = "advicesMenuItem".localized
    static let LegalNotesMenuItem = "legalNotesMenuItem".localized
    static let FacebookShareTitle = "facebookShareTitle".localized
    static let ShareSingleWineDescription = "shareSingleWineDescription".localized
    static let MailSubject = "mailSubject".localized
    static let SmsSingleWineText = "smsSingleWineText".localized
    static let ShareWineListText = "shareWineListText".localized
    static let SmsWineListText = "smsWineListText".localized
    static let TutorialFirstTitle = "tutorialFirstTitle".localized
    static let TutorialSecondTitle = "tutorialSecondTitle".localized
    static let TutorialThirdTitle = "tutorialThirdTitle".localized
    static let TutorialFourthTitle = "tutorialFourthTitle".localized
    static let TutorialFirstDescription = "tutorialFirstDescription".localized
    static let TutorialSecondDescription = "tutorialSecondDescription".localized
    static let TutorialThirdPage = "tutorialThirdPage".localized
    static let TutorialFourthDescription = "tutorialFourthDescription".localized
    static let TutorialSkipButton = "tutorialSkipButton".localized
}
