//
//  AdviceViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/23/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit

class AdviceViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var adviceNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var adviceName = String()
    var adviceDescription = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print(adviceDescription)
//        print(adviceName)
        webView.loadHTMLString(adviceDescription, baseURL: nil)
        adviceNameLabel.text = adviceName.uppercased()
        let frame = CGRect(x: titleLabel.frame.origin.x, y: titleLabel.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0)
        let lineSeparator = UIView(frame: frame)
        lineSeparator.backgroundColor = GlobalConstants.LightGrayColor
        titleLabel.addSubview(lineSeparator)
        titleLabel.text = LocalizationConstants.AdvicesTitleText.uppercased()
        
        let backButtonImage = UIImageView(image: UIImage(named:GlobalConstants.BackButtonImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        backButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size: backButtonImage.frame.size)
        self.navigationController?.view.addSubview(backButtonImage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
