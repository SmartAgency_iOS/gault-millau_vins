//
//  ViewController.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/3/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import DropDown
import CoreData
import FirebaseMessaging


class HomeViewController: UIViewController, SINavigationBar, SILoadingViewRenderer, UITextFieldDelegate {
    @IBOutlet weak var wineCountLabel: UILabel!
    @IBOutlet weak var regionView: UIButton!
    @IBOutlet weak var colorView: UIButton!
    @IBOutlet weak var vintageView: UIButton!
    @IBOutlet weak var wineCriteriaField: UITextField!
    @IBOutlet weak var regionArrow: UIImageView!
    @IBOutlet weak var colorArrow: UIImageView!
    @IBOutlet weak var vintageArrow: UIImageView!
    @IBOutlet weak var chooseCriteriaLabel: UILabel!
    @IBOutlet weak var additionalCriteriaLabel: UILabel!
    
    let regionDropDown = DropDown()
    let colorDropDown = DropDown()
    let vintageDropDown = DropDown()
    var regionCriteria:String?, colorCriteria:String?, vintageCriteria:String?, freeTextCriteria:String?
    var burgerImage:UIImageView!
    var wineCount:Int!
    var splashImageView:UIImageView!
    var infoButtonImage:UIImageView!
    var header:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barWithTint(UIColor.white, translucent: false, backButtonOptions: SINavigationBarBackButtonOptions(backButtonType: .none))
        self.setNavigationBarItem()
        let infoButton = UIBarButtonItem(title: "Info", style: .plain, target: self, action: #selector(displayTutorialViewController(sender:)))
        self.navigationItem.rightBarButtonItem = infoButton
        wineCount = -1

        let language = NSLocale.current.languageCode
        print("Language: \(language!)")
        if language == "fr" {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/en")
            Messaging.messaging().subscribe(toTopic: "/topics/fr")
        } else {
            Messaging.messaging().unsubscribe(fromTopic: "/topics/fr")
            Messaging.messaging().subscribe(toTopic: "/topics/en")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fetchWineTypeByRegion(notification:)), name: NSNotification.Name(rawValue:"fetchWineTypeByRegion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fetchVintageByRegion(notification:)), name: NSNotification.Name(rawValue:"fetchVintageByRegion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fetchVintageByRegionAndWineType(notification:)), name: NSNotification.Name(rawValue:"fetchVintageByRegionAndWineType"), object: nil)

        DataManager.shared.getWineCount() {
            (result:Int) in
            let string = LocalizationConstants.Wines
            self.wineCountLabel.text = "\(result) \(string)"
            self.wineCount = result
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.DataLoadedNotificationName), object: nil)
        }

        DataManager.shared.populateRegionDataSource() {
            (completion: [String]) in
            self.regionDropDown.dataSource = completion
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.DataLoadedNotificationName), object: nil)
        }

        DataManager.shared.populateColorDataSource() {
            (completion:[String]) in
            self.colorDropDown.dataSource = completion
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.DataLoadedNotificationName), object: nil)
        }

        DataManager.shared.popultaeVintageDataSourceForAllWines() {
            (completion: [String]) in
            self.vintageDropDown.dataSource = completion.sorted().reversed()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: GlobalConstants.DataLoadedNotificationName), object: nil)
        }

        DataManager.shared.addressesWithoutCoordinates() {
            (completion:Int) in
            print("ADDRESSES WITHOUT COORDINATES: \(completion)")
        }

        DataManager.shared.allAddresses() {
            (completion:Int) in
            print("ALL ADDRESSES: \(completion)")
        }

        setupDropDowns()
        setupUI()
        header = UIImageView(image: UIImage(named:GlobalConstants.HeaderMenuImageName))
        header.frame = CGRect(x: (self.navigationController?.navigationBar.frame.origin.x)!, y: (self.navigationController?.navigationBar.frame.origin.y)!, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)! + 20)
        self.navigationController?.view.addSubview(header)
        
        burgerImage = UIImageView(image: UIImage(named:GlobalConstants.BurgerMenuImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        burgerImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.origin.x)! + 20, y: (self.navigationController?.navigationBar.frame.origin.y)!), size: burgerImage.frame.size)
        header.addSubview(burgerImage)

        infoButtonImage = UIImageView(image: UIImage(named: "infoButtonImage")?.withRenderingMode(.alwaysOriginal))
        infoButtonImage.frame = CGRect(origin: CGPoint(x: (self.navigationController?.navigationBar.frame.size.width)! - 30, y: (self.navigationController?.navigationBar.frame.origin.y)! + 20), size:  infoButtonImage.frame.size)
        self.navigationController?.view.addSubview(infoButtonImage)

        regionCriteria = ""
        vintageCriteria = ""
        colorCriteria = ""
        freeTextCriteria = ""
        
        NotificationCenter.default.addObserver(self, selector: #selector(didLoadData(notification:)), name: NSNotification.Name(rawValue: GlobalConstants.DataLoadedNotificationName), object: nil)
    }
    
    override func viewDidAppear(_ animated:Bool) {
        super.viewDidAppear(true)
        self.reloadInputViews()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        burgerImage?.removeFromSuperview()
        infoButtonImage.removeFromSuperview()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: GlobalConstants.DataLoadedNotificationName), object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"fetchWineTypeByRegion"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"fetchVintageByRegion"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"fetchVintageByRegionAndWineType"), object: nil)
        self.view.resignFirstResponder()
        self.view.endEditing(true)
    }

    @objc func displayTutorialViewController(sender:UIBarButtonItem) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "TutorialViewController")
        self.present(viewController!, animated: true, completion: nil)
    }
    
    func setupDropDowns() {
        regionDropDown.textFont = UIFont.systemFont(ofSize: 13.0)
        vintageDropDown.textFont = UIFont.systemFont(ofSize: 13.0)
        colorDropDown.textFont = UIFont.systemFont(ofSize: 13.0)
        regionDropDown.anchorView = regionView
        regionDropDown.bottomOffset = CGPoint(x: 0, y: regionView.bounds.height)
        regionDropDown.direction = .bottom
        regionDropDown.cellNib = UINib(nibName: GlobalConstants.DropDownCellNibName, bundle: Bundle(for: DropDownCell.self))
        regionDropDown.cellHeight = GlobalConstants.DropDownCellHeight
        regionDropDown.backgroundColor = UIColor.white
        regionDropDown.textColor = GlobalConstants.RedColor
        regionDropDown.customCellConfiguration =  { (index: Index, item: String, cell: DropDownCell) -> Void in
            let frame = CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: cell.frame.size.width - 60, height: 1)
            let line = UIView(frame: frame)
            line.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(line)
            cell.optionLabel.alignmentRect(forFrame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.origin.y - 10, width: 300, height: 44))
        }
        
        regionDropDown.selectionAction = { [unowned self] (index, item) in
            self.regionView.setTitle(item, for: .normal)
            self.regionCriteria = item
            self.regionView.backgroundColor = UIColor.white
            self.regionView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.regionArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"fetchWineTypeByRegion"), object: item)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"fetchVintageByRegion"), object: item)
        }
        
        regionDropDown.cancelAction = { [unowned self] in
            self.regionView.backgroundColor = UIColor.white
            self.regionView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.regionArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        
        vintageDropDown.anchorView = vintageView
        vintageDropDown.bottomOffset = CGPoint(x: 0, y: vintageView.bounds.height)
        vintageDropDown.direction = .bottom
        vintageDropDown.cellNib = UINib(nibName: GlobalConstants.DropDownCellNibName, bundle: Bundle(for: DropDownCell.self))
        vintageDropDown.customCellConfiguration = nil
        vintageDropDown.selectionAction = { [unowned self] (index, item) in
            self.vintageView.setTitle(item, for: .normal)
            self.vintageCriteria = item
            self.vintageView.backgroundColor = UIColor.white
            self.vintageView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.vintageArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        
        vintageDropDown.cancelAction = { [unowned self] in
            self.vintageView.backgroundColor = UIColor.white
            self.vintageView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.vintageArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        
        vintageDropDown.cellHeight = GlobalConstants.DropDownCellHeight
        vintageDropDown.backgroundColor = UIColor.white
        vintageDropDown.textColor = GlobalConstants.RedColor
        vintageDropDown.customCellConfiguration =  { (index: Index, item: String, cell: DropDownCell) -> Void in
            let frame = CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: cell.frame.size.width - 60, height: 1)
            let line = UIView(frame: frame)
            line.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(line)
            cell.optionLabel.alignmentRect(forFrame: CGRect(x: cell.frame.origin.x + 30, y: cell.frame.origin.y - 10, width: 300, height: 44))
        }
        
        colorDropDown.anchorView = colorView
        colorDropDown.bottomOffset = CGPoint(x: 0, y: colorView.bounds.height)
        colorDropDown.direction = .bottom
        colorDropDown.cellNib = UINib(nibName: GlobalConstants.DropDownCellNibName, bundle: Bundle(for: DropDownCell.self))
        colorDropDown.customCellConfiguration = nil
        colorDropDown.selectionAction = { [unowned self] (index, item) in
            self.colorView.setTitle(item, for: .normal)
            self.colorCriteria = item
            self.colorView.backgroundColor = UIColor.white
            self.colorView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.colorArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchVintageByRegionAndWineType"), object: item)
        }
        
        colorDropDown.cancelAction = { [unowned self] in
            self.colorView.backgroundColor = UIColor.white
            self.colorView.setTitleColor(GlobalConstants.RedColor, for: .normal)
            self.colorArrow.image = UIImage(named: GlobalConstants.TriangleDownRedImageName)
        }
        colorDropDown.cellHeight = GlobalConstants.DropDownCellHeight
        colorDropDown.backgroundColor = UIColor.white
        colorDropDown.textColor = GlobalConstants.RedColor
        colorDropDown.customCellConfiguration =  { (index: Index, item: String, cell: DropDownCell) -> Void in
            let frame = CGRect(x: cell.frame.origin.x + 30, y: cell.frame.size.height - 1, width: cell.frame.size.width - 60, height: 1)
            let line = UIView(frame: frame)
            line.backgroundColor = GlobalConstants.RedColor
            cell.addSubview(line)
        }

    }
    
    func setupUI() {
        view.layoutIfNeeded()
        chooseCriteriaLabel.text = LocalizationConstants.ChooseCriteriaLabelText
        additionalCriteriaLabel.text = LocalizationConstants.AdditionalCriteriaLabelText
        wineCountLabel.backgroundColor = UIColor.white
        let line = UIView(frame: CGRect(x: wineCountLabel.frame.origin.x, y: wineCountLabel.frame.size.height - 1, width: GlobalConstants.ScreenWidth, height: 1.0))
        line.backgroundColor = GlobalConstants.LightGrayColor
        wineCriteriaField.placeholder = LocalizationConstants.WinePlaceholed
        wineCountLabel.addSubview(line)
        regionView.layer.borderColor = GlobalConstants.RedColorCgValue
        regionView.backgroundColor = UIColor.white
        regionView.layer.borderWidth = 2
        regionView.setTitle(LocalizationConstants.RegionOfOrigin, for: .normal)
        
        vintageView.layer.borderColor = GlobalConstants.RedColorCgValue
        vintageView.backgroundColor = UIColor.white
        vintageView.layer.borderWidth = 2
        vintageView.setTitle(LocalizationConstants.Vintage, for: .normal)
        
        colorView.setTitle(LocalizationConstants.WineColor, for: .normal)
        colorView.layer.borderColor = GlobalConstants.RedColorCgValue
        colorView.backgroundColor = UIColor.white
        colorView.layer.borderWidth = 2
        
        wineCriteriaField.layer.borderColor = GlobalConstants.RedColorCgValue
        wineCriteriaField.layer.borderWidth = 2.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didLoadData(notification:NSNotification) {
        if wineCount != -1 && regionDropDown.dataSource.count > 0 && colorDropDown.dataSource.count > 0 && vintageDropDown.dataSource.count > 0 {
            self.hideLoadingView()
        }
    }
    
    
    @IBAction func searchForWine(_ sender: UIButton) {
        
        }
    
    @IBAction func chooseRegion(_ sender:UIButton) {
        regionDropDown.show()
        
        if(!regionDropDown.isHidden) {
            regionView.backgroundColor = GlobalConstants.RedColor
            regionView.setTitleColor(UIColor.white, for: .normal)
            regionArrow.image = UIImage(named: GlobalConstants.TriangleDownWiteImageName)
        }
    }
    
    @IBAction func chooseColor(_ sender: Any) {
        colorDropDown.show()
        
        if(!colorDropDown.isHidden) {
            colorView.backgroundColor = GlobalConstants.RedColor
            colorView.setTitleColor(UIColor.white, for: .normal)
            colorArrow.image = UIImage(named: GlobalConstants.TriangleDownWiteImageName)
        }
    }
    
    @IBAction func chooseVintage(_ sender: Any) {
        vintageDropDown.show()
        
        if(!vintageDropDown.isHidden) {
            vintageView.backgroundColor = GlobalConstants.RedColor
            vintageView.setTitleColor(UIColor.white, for: .normal)
            vintageArrow.image = UIImage(named: GlobalConstants.TriangleDownWiteImageName)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GlobalConstants.SearchForWineByCriteriaSegueIdentifier {
            if wineCriteriaField.text != "" {
                freeTextCriteria = wineCriteriaField.text
            }
        
            if let destinationVC = segue.destination as? WineListViewControllerTableViewController {
                destinationVC.dataSource = []
                destinationVC.keyword = freeTextCriteria
                destinationVC.regionCriteria = regionCriteria
                destinationVC.colorCriteria = colorCriteria
                destinationVC.vintageCriteria = vintageCriteria
                DataManager.shared.fetchWineByCriteria(wineRegion: regionCriteria, wineColor: colorCriteria, wineVintage: vintageCriteria, freeText: freeTextCriteria) {
                    (completion:[Wine]) in
                    destinationVC.dataSource = completion
                    print(destinationVC.dataSource.count)
                }
            }
        }
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 64
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        return true
    }

    @objc func fetchWineTypeByRegion(notification:NSNotification) {
        DataManager.shared.fetchWineTypeByRegion(regionName:notification.object as! String) {
            (result:[String]) in
            self.colorDropDown.dataSource = result

            if self.colorCriteria != "" && self.colorCriteria != nil {
                if !result.contains(self.colorCriteria!) {
                    self.colorCriteria = ""
                    self.colorView.setTitle(LocalizationConstants.WineColor, for: .normal)
                }
            }
        }
    }

    @objc func fetchVintageByRegion(notification:NSNotification) {
        DataManager.shared.fetchVintageByRegion(regionName: notification.object as! String) {
            (result:[String]) in
            self.vintageDropDown.dataSource = result.sorted().reversed()

            if self.vintageCriteria != "" && self.vintageCriteria != nil {
                if !result.contains(self.vintageCriteria!) {
                    self.vintageCriteria = ""
                    self.vintageView.setTitle(LocalizationConstants.Vintage, for: .normal)
                }
            }
        }
    }

    @objc func fetchVintageByRegionAndWineType(notification:NSNotification) {
        DataManager.shared.fetchVintageByRegionAndWineType(regionName: regionCriteria, wineType:notification.object as! String) {
            (result:[String]) in
            self.vintageDropDown.dataSource = result.sorted().reversed()

            if self.vintageCriteria != "" && self.vintageCriteria != nil {
                if !result.contains(self.vintageCriteria!) {
                    self.vintageCriteria = ""
                    self.vintageView.setTitle(LocalizationConstants.Vintage, for: .normal)
                }
            }
        }
    }

}

