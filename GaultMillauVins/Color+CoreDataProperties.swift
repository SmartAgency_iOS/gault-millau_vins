//
//  Color+CoreDataProperties.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/4/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import Foundation
import CoreData


extension Color {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Color> {
        return NSFetchRequest<Color>(entityName: DbConstants.ColorEntityName);
    }

    @NSManaged public var color_name: String
    @NSManaged public var color_wine: NSSet

}

// MARK: Generated accessors for color_wine
extension Color {

    @objc(addColor_wineObject:)
    @NSManaged public func addToColor_wine(_ value: Wine)

    @objc(removeColor_wineObject:)
    @NSManaged public func removeFromColor_wine(_ value: Wine)

    @objc(addColor_wine:)
    @NSManaged public func addToColor_wine(_ values: NSSet)

    @objc(removeColor_wine:)
    @NSManaged public func removeFromColor_wine(_ values: NSSet)

}
