//
//  AppDelegate.swift
//  GaultMillauVins
//
//  Created by Veronika Velkova on 11/3/16.
//  Copyright © 2016 Veronika Velkova. All rights reserved.
//

import UIKit
import CoreData
import DropDown
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Branch


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    lazy var applicationDocumentsDirectory: NSURL = {
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Override point for customization after application launch.
//        print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last! as String)
        let defaults = UserDefaults.standard
        let isPreloaded = defaults.bool(forKey: "isPreloaded")
        if !isPreloaded {
            self.copyFile()
            DataParser.shared.populateDb()
            defaults.set(true, forKey: "isPreloaded")
        }

        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil {
                print("params: %@", params?.description as Any)
            }
        })

        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject] != nil {
            let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject]
            var message:String!
            var title = "Vins"
            if let aps = userInfo?["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSString {
                    message = String(alert)
                } else {
                    let alert = userInfo?["aps"] as? NSDictionary
                    let pushInfo = alert?["alert"] as? NSDictionary
                    message = pushInfo?["body"] as? String
                    title = (pushInfo?["title"] as? String)!

                }
            }


            let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
            topWindow.rootViewController = UIViewController()
            topWindow.windowLevel = UIWindow.Level.alert + 1
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(action: UIAlertAction) -> Void in
                topWindow.isHidden = true
            })

            alertController.addAction(defaultAction)
            topWindow.makeKeyAndVisible()
            topWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }

        DropDown.startListeningToKeyboard()
        self.showLegalNotesStoryboard()
        self.showAdvicesStoryboard()
        self.showVintageStoryboard()
        self.showMealsWinesStoryboard()
        self.showFavouritesStoryboard()
        //self.showMainStoryboard()

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let splashViewController = storyboard.instantiateViewController(withIdentifier: "SplashScreen") as! SplashScreenViewController
        self.window?.rootViewController = splashViewController

        UITableView.appearance().separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        UITableView.appearance().separatorColor = UIColor.clear
        UITableViewCell.appearance().preservesSuperviewLayoutMargins = false
        UITableViewCell.appearance().layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        UITableViewCell.appearance().separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })

            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self

        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        FirebaseApp.configure()

        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }


        return true
    }
    
    private func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        Branch.getInstance().handleDeepLink(url);
        print("URL \(url)")
        let urlString = url.absoluteString
        let queryArray = urlString.components(separatedBy: "/")
        let query = queryArray[2]

        if query == "wine" {
            let data = urlString.components(separatedBy: "/")
            if data.count >= 3 {
                let startIndex = urlString.range(of: "wine://wine/")
                if let upperBound = startIndex?.upperBound {
                    let parameter = String(urlString[..<upperBound])
                    self.showWineDetailsStoryboard(wineId: parameter)
                }
            }
        } else if query == "designation" {
            let data = urlString.components(separatedBy: "/")
            if data.count >= 3 {

                let startIndex = urlString.range(of: "wine://designation/")
                if let upperBound = startIndex?.upperBound {
                    let parameter = String(urlString[..<upperBound])
                    self.showWineForMealDetail(wineId: parameter)
                }
            }
        } else if query == "search" {
            let data = urlString.components(separatedBy: "/")
            if data.count >= 3 {
                let startIndex = urlString.range(of: "wine://search/")
                if let upperBound = startIndex?.upperBound {
                    let parameter = String(urlString[..<upperBound])
                    self.showSearchStoryboard(searchString: parameter)
                }
            }

        }

        return true
    }

    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        return true
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        DataBaseContext.shared.saveContext()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }

        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    // [START connect_to_fcm]
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = NSString(format: "%@", deviceToken as CVarArg)
        token = token.replacingOccurrences(of: "<", with: "") as NSString
        token = token.replacingOccurrences(of: ">", with: "") as NSString
        token = token.replacingOccurrences(of: " ", with: "") as NSString
        print("APNs token retrieved: \(token)")

        // With swizzling disabled you must set the APNs token here.
        // FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
    }

    func showMainStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showFavouritesStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "FavouritesNavigation") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showMealsWinesStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "MealsWinesNavigation") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showVintageStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "VintageNavigation") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showAdvicesStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "AdvicesNavigation") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }

    func showLegalNotesStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "LegalNotesNavigation") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }

    func showWineDetailsStoryboard(wineId:String) {
        var wine = Wine(context: DataBaseContext.shared.managedObjectContext)
        let id = DataBaseContext.shared.persistentStoreCoordinator.managedObjectID(forURIRepresentation: URL(string: wineId)!)
        do {
            let object = try DataBaseContext.shared.managedObjectContext.existingObject(with: id!)
            wine = object as! Wine
        } catch {

        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuViewController = MenuViewController()
        let navigation = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController

        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(navigation, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: navigation, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
        }

        let rootView = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        navigation.pushViewController(rootView, animated: false)

        let viewController = storyboard.instantiateViewController(withIdentifier: "WineDetailViewController") as! WineDetailViewController
        viewController.wine = wine

        navigation.pushViewController(viewController, animated: true)
    }

    func showWineForMealDetail(wineId:String) {
        var wine = Wine(context: DataBaseContext.shared.managedObjectContext)
        let id = DataBaseContext.shared.persistentStoreCoordinator.managedObjectID(forURIRepresentation: URL(string: wineId)!)
        do {
            let object = try DataBaseContext.shared.managedObjectContext.existingObject(with: id!)
            wine = object as! Wine
        } catch {

        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuViewController = MenuViewController()
        let navigation = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController

        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(navigation, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: navigation, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
        }

        let rootView = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        navigation.pushViewController(rootView, animated: false)

        let viewController = storyboard.instantiateViewController(withIdentifier: "WineDetailsForMealViewController") as! WineDetailsForMealViewController
        viewController.wine = wine

        navigation.pushViewController(viewController, animated: true)
    }

    func showSearchStoryboard(searchString:String) {
        var parameters = [String]()
        parameters = searchString.components(separatedBy: "=")
        var regionCriteria = "", colorCriteria = "", vintageCriteria = "", key = ""

        for param in parameters {
            if param == "region" {
                regionCriteria = parameters[parameters.index(of: "region")! + 1].removingPercentEncoding!
            } else if param == "color" {
                colorCriteria = parameters[parameters.index(of: "color")! + 1].removingPercentEncoding!
            } else if param == "vintage" {
                vintageCriteria = parameters[parameters.index(of: "vintage")! + 1].removingPercentEncoding!
            } else if param == "key" {
                key = parameters[parameters.index(of: "key")! + 1].removingPercentEncoding!
            }
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuViewController = MenuViewController()
        let navigation = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController

        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(navigation, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: navigation, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
        }

        let rootView = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        navigation.pushViewController(rootView, animated: false)

        let viewController = storyboard.instantiateViewController(withIdentifier: "WineListViewControllerTableViewController") as! WineListViewControllerTableViewController

        DataManager.shared.fetchWineByCriteria(wineRegion: regionCriteria, wineColor: colorCriteria, wineVintage: vintageCriteria, freeText: key) {
            (completion:[Wine]) in
            viewController.dataSource = completion
        }

        navigation.pushViewController(viewController, animated: true)

    }

    func copyFile(){
        let sourceSqliteURLs = [Bundle.main.url(forResource: "GaultMillauVins", withExtension: "sqlite")!, Bundle.main.url(forResource: "GaultMillauVins", withExtension: "sqlite-wal")!, Bundle.main.url(forResource: "GaultMillauVins", withExtension: "sqlite-shm")!]
        
        let destSqliteURLs = [self.applicationDocumentsDirectory.appendingPathComponent("GaultMillauVins.sqlite"),
                              self.applicationDocumentsDirectory.appendingPathComponent("GaultMillauVins.sqlite-wal"),
                              self.applicationDocumentsDirectory.appendingPathComponent("GaultMillauVins.sqlite-shm")]
        
        for index in 0 ..< sourceSqliteURLs.count {
            do {
               try FileManager.default.copyItem(at: sourceSqliteURLs[index], to: destSqliteURLs[index]!)
            } catch {
                print(error)
            }
        }
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)
        var message:String!
        var title = "Vins"
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSString {
                message = String(alert)
            } else {
                let alert = userInfo["aps"] as? NSDictionary
                if let pushInfo = alert?["alert"] as? NSDictionary {
                    message = pushInfo["body"] as! String
                    title = (pushInfo["title"] as! String)
                }
            }
        }

        let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindow.Level.alert + 1
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(action: UIAlertAction) -> Void in
            topWindow.isHidden = true
        })

        alertController.addAction(defaultAction)
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alertController, animated: true, completion: nil)

        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)

        var message:String!
        var title = "Vins"
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSString {
                message = String(alert)
            } else {
                let alert = userInfo["aps"] as? NSDictionary
                if let pushInfo = alert?["alert"] as? NSDictionary {
                    message = pushInfo["body"] as! String
                    title = (pushInfo["title"] as! String)
                }
            }
        }


        let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindow.Level.alert + 1
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(action: UIAlertAction) -> Void in
            topWindow.isHidden = true
        })

        alertController.addAction(defaultAction)
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alertController, animated: true, completion: nil)

        completionHandler()
    }
}
// [END ios_10_message_handling]
// [START ios_10_data_message_handling]
extension AppDelegate : MessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
}


